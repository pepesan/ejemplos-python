if __name__ == '__main__':

    # lambda definition
    add = lambda x, y: x + y
    # Using a lambda
    print(add(2, 3))  # Output: 5

    # Map function
    numbers = [1, 2, 3, 4]
    doubles = map(lambda x: x * 2, numbers)
    # Multiplies each number by 2
    print(list(doubles))  # Output: [2, 4, 6, 8]

    # Filter function
    numbers = [1, 2, 3, 4]
    even = filter(lambda x: x % 2 == 0, numbers)
    # Only even numbers
    print(list(even))  # Output: [2, 4]

    # Reduce function
    from functools import reduce

    numbers = [1, 2, 3, 4]
    sum_result = reduce(lambda x, y: x + y,
                        numbers)  # Sums all the numbers
    print(sum_result)  # Output: 10



    def apply_operation(func, a, b):
        return func(a, b)


    # Passing a Function that adds two numbers
    result = apply_operation(lambda x, y: x + y, 3, 5)
    print(result)  # Output: 8


    # returing a function
    def multiply_by(factor):
        return lambda x: x * factor


    # Create a function that multiplies by 3
    multiply_by_3 = multiply_by(3)
    print(multiply_by_3(5))  # Output: 15