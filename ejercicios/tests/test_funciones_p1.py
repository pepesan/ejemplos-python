from ejercicios.funciones_prueba import resta


def test_resta_simple():
    n1 = 2
    n2 = 1
    resEsperado = 1
    res = resta(n1, n2)
    assert res == resEsperado
