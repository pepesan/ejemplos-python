if __name__ == '__main__':
    # -*- coding: utf-8 -*-

    # Define a dictionary
    my_dict = {
        'Name': 'Zara',
        'Age': 7,
        'Class': 'First'
    }

    # Access dictionary elements
    print("dict['Name']: ", my_dict['Name'])
    print("dict['Age']: ", my_dict['Age'])

    # Update an existing entry
    my_dict['Age'] = 8
    # Add a new entry
    my_dict['School'] = "DPS School"
    my_dict['Last Name'] = "Petterson"

    # Print the entire dictionary
    print(my_dict)
    # Return the keys
    print(my_dict.keys())
    # Return the values
    print(my_dict.values())
    # Access a specific key
    print(my_dict['Last Name'])

    # Iterate through the dictionary
    for key_name in my_dict:
        print(key_name)
        print(my_dict[key_name])

    # Add an array as a value
    my_dict['my_array'] = [1, 2]
    print("Deleting a key")
    # Remove an entry with the key 'Name'
    del my_dict['Name']
    print(my_dict)
    # Remove all entries in the dictionary
    my_dict.clear()
    print(my_dict)
    # Delete the entire dictionary
    del my_dict

    # Define another dictionary
    my_dict = {'Name': 'Zara', 'Age': 7}
    # Get the length of the dictionary
    print(len(my_dict))

    # Add a nested dictionary
    my_dict['Last Names'] = {
        'first': "Lights",
        'second': "Off"
    }

    # Print the dictionary and access nested values
    print(my_dict)
    print(my_dict['Last Names']['first'])
