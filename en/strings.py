if __name__ == '__main__':
    # -*- coding: utf-8 -*-

    # Examples of strings in Python
    string = "Lorem Ipsum no cuetator cosas"
    string2 = " cx xc xc xc  another string"

    # 1. Length of the string
    print("Length of the string:", len(string))

    # 2. Convert to uppercase
    print("String in uppercase:", string.upper())

    # 3. Convert to lowercase
    print("String in lowercase:", string.lower())

    # 4. Capitalize (first letter in uppercase)
    print("String with capitalization:", string.capitalize())

    # 5. Title format (first letter of each word in uppercase)
    print("String in title format:", string.title())

    # 6. Remove leading and trailing whitespaces
    string_with_spaces = "    text with spaces    "
    print("String with spaces removed:", string_with_spaces.strip())

    # 7. Concatenate two strings
    string3 = string + string2
    print("String concatenation:", string3)

    # 8. Find a substring
    substring_index = string3.find("another")
    print("Index of 'another':", substring_index)

    # 9. Replace a substring with another
    replaced_string = string3.replace("another", "this")
    print("Replaced string:", replaced_string)

    # 10. Replace a substring a specific number of times
    partially_replaced_string = string3.replace("xc", "22", 2)
    print("Partially replaced string:", partially_replaced_string)

    # 11. Find the index of a substring (throws an error if not found)
    try:
        index = string3.index("xc")
        print("Index of 'xc':", index)
    except ValueError:
        print("Substring not found")

    # 12. Count the number of occurrences of a substring
    count = string3.count("xc")
    print("Number of times 'xc' appears:", count)

    # 13. Check if a string starts with another string
    print("Does string3 start with 'Lorem'?", string3.startswith("Lorem"))

    # 14. Check if a string ends with another string
    print("Does string3 end with 'string'?", string3.endswith("string"))

    # 15. Convert string into a list of words using a delimiter (split)
    word_list = string3.split()
    print("List of words:", word_list)

    # 16. Join a list of words into a single string with a delimiter (join)
    joined_string = "-".join(word_list)
    print("String joined with hyphens:", joined_string)

    # 17. Check if a string is numeric
    numeric_string = "12345"
    print("Is the string '12345' numeric?", numeric_string.isdigit())

    # 18. Check if a string is alphabetic
    alphabetic_string = "abcdef"
    print("Is the string 'abcdef' alphabetic?", alphabetic_string.isalpha())

    # 19. Check if a string is alphanumeric
    alphanumeric_string = "abc123"
    print("Is the string 'abc123' alphanumeric?", alphanumeric_string.isalnum())

    # 20. Pad the string to the left with zeros up to a specific length
    zero_padded_string = numeric_string.zfill(10)
    print("String padded with zeros:", zero_padded_string)

    # 21. Convert to title format and verify if it is in title format
    title_string = "hello world"
    title_string = title_string.title()
    print("String in title format:", title_string)
    print("Is it in title format?", title_string.istitle())

    # 22. Example of centering, left alignment, and right alignment
    print("Centered:", string.center(40, "*"))
    print("Left-aligned:", string.ljust(40, "-"))
    print("Right-aligned:", string.rjust(40, "-"))

    # 23. Get a substring using slicing
    substring = string[0:5]
    print("Substring (first 5 characters):", substring)

    # 24. Reverse a string
    reversed_string = string[::-1]
    print("Reversed string:", reversed_string)

    # 25. Use `format` to insert values into a string
    name = "Juan"
    age = 25
    message = "My name is {} and I am {} years old.".format(name, age)
    print("Formatted message:", message)

    # 26. Use f-strings to format the string (Python 3.6+)
    fstring_message = f"My name is {name} and I am {age} years old."
    print("Message with f-strings:", fstring_message)
