if __name__ == '__main__':
    # classes2.py

    class Person:
        def __init__(self, name=None, id_number=None, phone=None, email=None, address=None):
            self.name = name
            self.id_number = id_number
            self.phone = phone
            self.email = email
            self.address = address
            self.addresses = []
            if address:
                self.addresses.append(address)

        def setAddress(self, address):
            self.address = address
            self.addresses.append(address)


    # Creating an object of the Person class named pepito
    pepito = Person(name="Pepito", id_number="123456789", phone="9876543210", email="pepito@example.com")

    # Modify the address for pepito using the setAddress method
    pepito.setAddress("456 New Street")

    # Verifying the data has been modified correctly
    print("Person Name:", pepito.name)
    print("ID Number:", pepito.id_number)
    print("Phone:", pepito.phone)
    print("Email:", pepito.email)
    print("Current Address:", pepito.address)
    print("Addresses History:", pepito.addresses)

    # Initializing multiple Person objects with different numbers of parameters
    person1 = Person(name="Ana")
    person2 = Person(name="Carlos", id_number="654321")
    person3 = Person(name="Luis", id_number="987654", phone="1234567890", email="luis@example.com",
                     address="789 Old Street")

    # Printing to verify data
    print("\nPerson 1:", vars(person1))
    print("Person 2:", vars(person2))
    print("Person 3:", vars(person3))
