class Person:
    def __init__(self, name, age):
        """
        Initializes a Person instance.
        :param name: str, the name of the person
        :param age: int, the age of the person
        """
        self.name = name
        self.age = age

    def greet(self):
        """
        Prints a greeting message including the person's name and age.
        """
        print(f"Hello, my name is {self.name} and I am {self.age} years old.")


def is_adult(age):
    """
    Determines if the given age qualifies as an adult (18 or older).
    :param age: int, the age to check
    :return: bool, True if age is 18 or older, False otherwise
    """
    return age >= 18