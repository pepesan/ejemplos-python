#Instalación de SQLite3
sudo apt install sqlite3

##Apertura de bbdd
sqlite3 author_book_publisher.db
###Salida de sqlite3
.exit

## Generación de modelos desde metadatos de BBDD
pip install sqlacodegen
### ejecución
####Windows 
sqlacodegen sqlite:///author_book_publisher.db >> fichero_salida.py
####Linux
./generate_model.sh sqlite:///author_book_publisher.db >> fichero_salida.py

