if __name__ == '__main__':

    # Define the decorator
    def my_decorator(func):
        def wrapper():
            print("Before executing the function.")
            func()  # Call the original function
            print("After executing the function.")

        return wrapper


    # Apply the decorator to a function
    @my_decorator
    def greet():
        print("Hello!")


    greet()


    def repeat(n):
        def decorator(func):
            def wrapper(*args, **kwargs):
                for _ in range(n):
                    print(f"Executing {func.__name__}...")
                    func(*args, **kwargs)

            return wrapper

        return decorator


    @repeat(3)  # We want the function to execute 3 times
    def greet(name):
        print(f"Hello, {name}!")


    greet("Anna")


    def debug(f):
        def new_function(a, b):
            print("Function called!")
            return f(a, b)

        return new_function


    @debug
    def add(a, b):
        return a + b

    @debug
    def multiply(a, b):
        return a * b


    print(add(7, 5))
    print(multiply(2, 3))


    def my_log(f):
        def new_function(x):
            print("Function called!")
            fx = f(x)
            print("Log the saved object")
            return fx

        return new_function

    @my_log
    def save(x):
        print("Object saved")
        return x

    print(save(7))


    class MyDecorator:
        def __init__(self, function):
            self.function = function

        def __call__(self):
            # Add code before the function call
            self.function()
            # Add code after the function call


    # Adding decorator to the class
    @MyDecorator
    def function():
        print("Something")


    function()


    class MyDecorator:
        def __init__(self, function):
            self.function = function

        def __call__(self, *args, **kwargs):
            # Add code before the function call
            self.function(*args, **kwargs)
            # Add code after the function call


    @MyDecorator
    def function(name, message='Hello'):
        print("{}, {}".format(message, name))


    function("Something", "hello")


    def decorator_function_with_arguments(arg1, arg2, arg3):
        def wrap(f):
            print("Inside wrap()")

            def wrapped_f(*args):
                print("Inside wrapped_f()")
                print("Decorator arguments:", arg1, arg2, arg3)
                f(*args)
                print("After f(*args)")

            return wrapped_f

        return wrap


    @decorator_function_with_arguments("hello", "world", 42)
    def say_hello(a1, a2, a3, a4):
        print('say_hello arguments:', a1, a2, a3, a4)


    print("After decoration")
    print("Preparing to call say_hello()")
    say_hello("say", "hello", "argument", "list")
    print("After first say_hello() call")
    say_hello("a", "different", "set of", "arguments")
    print("After second say_hello() call")


    def singleton(cls):
        instances = {}

        def get_instance():
            if cls not in instances:
                print("Instance not found")
                instances[cls] = cls()
            else:
                print("Instance found")
            return instances[cls]

        return get_instance


    @singleton
    class MyClass:
        def __init__(self):
            self.prop = ''


    c = MyClass()
    c.prop = "Hello"
    c = MyClass()
    print(c.prop)

    import functools
    import logging


    def create_logger():
        # Creates a log object and returns it

        logger = logging.getLogger("example_logger")
        logger.setLevel(logging.INFO)

        # Create the log file handler
        fh = logging.FileHandler("./test.log")

        fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        formatter = logging.Formatter(fmt)
        fh.setFormatter(formatter)

        # Add handler to logger object
        logger.addHandler(fh)
        return logger


    def exception(function):
        # A decorator that wraps the passed in function and logs exceptions should one occur

        @functools.wraps(function)
        def wrapper(*args, **kwargs):
            logger = create_logger()
            try:
                return function(*args, **kwargs)
            except:
                # Log the exception
                err = "There was an exception in "
                err += function.__name__
                logger.exception(err)
                print("Caught inside")
                # Re-raise the exception
                raise

        return wrapper


    @exception
    def zero_divide():
        1 / 0


    try:
        zero_divide()
    except:
        print("Caught outside")
