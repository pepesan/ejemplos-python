# -*- coding: utf-8 -*-
if __name__ == "__main__":
    list1 = ['physics', 'chemistry', 1997, 2000]
    list2 = [1, 2, 3, 4, 5, 5]
    list3 = ["a", "b", "c", "d"]

    print("list1[0]: ", list1[0]) #  'physics'
    print("list2[1:5]: ", list2[1:5]) # [2, 3, 4, 5]

    list2[0] = 1.1
    print("list2[0]: ", list2[0])
    print("len(list1)")
    print(len(list1))
    # [2, 3, 4, 5]
    newlist = ['physics', 'chemistry', 1997, 2000]

    print("Value available at index 2 : ")
    print(newlist[2])
    newlist[2] = 2001
    print("New value available at index 2 : ")
    print(newlist[2])

    x = [1, 2, 3, 4]
    # Añade un elemento al final del listado
    print(x)  # [1, 2, 3, 4]

    list1 = ['physics', 'chemistry', 1997, 2000]
    print(list1)
    del (list1[2])
    print("After deleting value at index 2 : ")
    print(list1)

    print("Suma de dos listados en uno: ")
    list2 = list1 + x
    print(list2)


    print("adding a value to a list, append")
    list3 = [1]
    list3.append(1)
    print(list3)


    Celsius = [25.0, 36.5, 37.3, 37.8]
    UnFahrenheit = ((float(9) / 5) * Celsius[0] + 32)
    print("Celsius", Celsius)
    print("UnFahrenheit", UnFahrenheit)
    # En cada una de las vueltas elemento vale en cada vuelta un valor distinto
    # Empezando por el primer valor de listado
    # Y terminando por el último valor del listado
    for element in Celsius:
        print(element)

    Fahrenheit = []
    for item in Celsius:
        i = ((float(9) / 5) * item + 32)
        Fahrenheit.append(i)
        print(i)
        print(Fahrenheit)

    print(Fahrenheit)
    """
    for (int i=0;i<10;i++){
        cosa 1; 
        cosa 2;
    }
    """
    # Rellena un listado con los datos modificados de otro listado
    Fahrenheit = [((float(9) / 5) * item + 32) for item in Celsius]
    print("Comprensión de listas")
    print(Fahrenheit)

    milist = [(x, y, z)
               for x in range(1, 30)
               for y in range(x, 30)
               for z in range(y, 30)
               if x ** 2 + y ** 2 == z ** 2]
    print(milist)

    listado = [1,2,3,4,5]
    print("Rangos")
    print(listado)
    # pillar el primer valor
    print(listado[0])
    # pillar desde el primer elemento hasta el final
    print(listado[0:])
    # pillar desde el primer elemento hasta tercer elemento
    # Primer número es inclusive, e segundo número es exclusive
    print(listado[0:3])
    # Devuelve el listado en inverso
    print(listado[::-1])

    # Seleccionamos con un rango de 1 a 50
    mi_listado = [x for x in range(1, 51)]
    print("range(1, 51)", mi_listado)
    # Seleccionamos con un rango de 0 a 49
    mi_listado = [x for x in range(50)]
    print(range(50), mi_listado)
    print(len(mi_listado))

    listado = [1, 2, 3]
    for x in listado:
        print(x)

    if (3 in listado):
        print("Está vivo!!!!!")

    holas = ['Hi!'] * 4  # ['Hi!', 'Hi!', 'Hi!', 'Hi!']
    print(4 * holas)
    # listado doble
    print("Listado Doble")
    listadoDoble = []
    listadoDoble.append([1,2])
    print(listadoDoble)
    listadoDoble.append([3,4])
    print(listadoDoble) # [[1,2],[3,4]]
    print("primera fila")
    print(listadoDoble[0])
    print("primer elemento de la primera fila")
    print(listadoDoble[0][0])
    for row in listadoDoble:
        print(row)
        for column in row:
            print(column)

    listadoDoble.append(["Hola", "Mundo"])
    print(listadoDoble)
    for fila in listadoDoble:
        print(fila)
        for columna in fila:
            print(columna)