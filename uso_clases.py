if __name__ == '__main__':
    class Point:
        def __init__(self, x=0, y=0):
            self.x = x
            self.y = y

        def __del__(self):
            class_name = self.__class__.__name__
            print(class_name, "destroyed")

    punto = Point()
    del punto

    def mi_function():
        mi_punto = Point()

    mi_function()