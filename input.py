if __name__ == '__main__':
    # Prompting the user to enter their name and age
    name = input("Enter your name: ")  # Takes a string input
    age = input("Enter your age: ")  # Takes a string input by default

    # Converting age to an integer
    # age = int(age)

    # Displaying the user's input
    print("\nThank you for your input!")
    print(f"Your name is {name} and you are {age} years old.")
