if __name__ == '__main__':
    import pandas as pd
    import numpy as np

    # Load the CSV file
    df = pd.read_csv("./clean.csv")
    print(df)

    # Display the first rows of the DataFrame
    print(df.head())
    # Display information about the DataFrame
    print(df.info())
    # Display the shape of the DataFrame
    print(df.shape)

    # Remove duplicates
    data = df.drop_duplicates()
    print('Remove duplicates')
    print(data)
    print(data.shape)

    # Remove rows with null values
    data = df.dropna()
    print('Remove null values')
    print(data)
    print(data.shape)

    # Fill null values with 0
    data = df.fillna(0)
    print('Fill null values with 0')
    print(data)
    print(data.shape)

    # Column modifications
    print('Column modifications')
    data = {'col1': [1, 2, 3, 4], 'col2': ['a', 'b', 'c', 'd']}
    df = pd.DataFrame(data)
    print(df)

    # Correct approach (modifies the original DataFrame using .loc)
    df.loc[df['col1'] > 2, 'col2'] = 'X'
    print(df)

    # Display column types
    print('Column types')
    df['col1'] = df['col1'].astype("int")
    print(df['col1'].dtype)
    print(df['col2'].dtype)

    # Create a DataFrame with NaN values
    data = {'A': [1, 2, np.nan, 4, 5]}
    df = pd.DataFrame(data)

    # Replace NaN with the mean of column 'A'
    df['A'] = df['A'].fillna(df['A'].mean())
    print(df)
