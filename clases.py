# -*- coding: utf-8 -*-

if __name__ == "__main__":
    class NombreClase:
        """Optional class documentation string"""
        # class_suite


    obj = NombreClase()


    class Empleado:
        """Clase que maneja los datos de un empleado"""

        # comentario
        def __init__(self):
            self.nombre = ""
            self.salario = 0
            # return self


    jorge = Empleado()
    print(jorge.nombre)
    jorge.nombre = "Jorge"
    print(jorge.nombre)
    print(jorge.salario)
    jorge.salario = 45000
    print(jorge.salario)

    juan = Empleado()
    juan.nombre = "Juan"
    juan.salario = 15000
    print(juan.nombre)
    print(juan.salario)


    class Empleado:
        """Clase que maneja los datos de un empleado"""

        # comentario
        def __init__(self, nombre="", salario=0):
            self.nombre = nombre
            self.salario = salario


    isabel = Empleado()
    print(isabel.salario)
    isabel = Empleado("Isabel")
    print(isabel.nombre)
    print(isabel.salario)
    isabel = Empleado("Isabel", 60000)
    print(isabel.nombre)
    print(isabel.salario)
    isabel = Empleado(salario=60000)
    print(isabel.salario)
    isabel = Empleado(nombre="Isabel")
    print(isabel.nombre)
    isabel = Empleado(nombre="Isabel", salario=60000)
    print(isabel.nombre)
    print(isabel.salario)
    isabel = Empleado(salario=60000, nombre="Isabel")
    print(isabel.nombre)
    print(isabel.salario)

    class Empleado:
        """Clase que maneja los datos de un empleado"""

        # comentario
        def __init__(self, nombre="", salario=0):
            self.nombre = nombre
            self.salario = salario

        def incrementaSalario(self, valor = 0):
            # self.salario = self.salario+valor
            # += -= *= /=
            self.salario += valor


    maria = Empleado(nombre="Maria", salario=75000)
    maria = Empleado(salario=75000)


    # maria.__salario= maria.__salario+10000
    def incrementaSalario(salario, incremento):
        return salario + incremento


    maria.salario = incrementaSalario(maria.salario, 10000)

    maria.incrementaSalario(10000)
    print(maria.salario)


    class Empleado:
        """Clase que maneja los datos de un empleado"""

        # comentario
        def __init__(self, nombre="", salario=0):
            self.nombre = nombre
            self.__salario = salario

        def setSalario(self, salario):
            self.__salario = salario

        def getSalario(self):
            return self.__salario


    maria = Empleado(nombre="Maria", salario=75000)
    ## Acceso a atributo privado (restringido)
    # print(maria.__salario)
    print(maria.getSalario())
    maria.setSalario(85000)
    print(maria.getSalario())


    class Empleado:
        """Clase que maneja los datos de un empleado"""

        # comentario
        def __init__(self, nombre="", salario=0):
            self.__nombre = nombre
            self.__salario = salario

        def incSalario(self, valor):
            # self.salario=self.salario+valor
            self.__salario += valor

        def setSalario(self, salario):
            self.__salario = salario

        def getSalario(self):
            return self.__salario

        def setNombre(self, nombre):
            self.__nombre = nombre

        def getNombre(self):
            return self.__nombre

        # toString
        def __str__(self):
            return 'Empleado [nombre:' + self.__nombre + ", salario:" + str(self.__salario) + "]"


    emp1 = Empleado("Zara", 2000)
    emp1.salario = 30000
    emp1.setSalario(35000)
    print(emp1.getSalario())
    print("__str__ == toString()")
    print(emp1)



    class Empleado:
        """Clase base de manejo de empleados"""
        # esta variable es estática y se comparte entre instancias de Empleado
        empCount = 0

        def __init__(self, name, salary):
            self.name = name
            self.salary = salary
            Empleado.empCount += 1

        @classmethod
        def presenta(cls, dato):
            print("Hola, me llamo ", dato, "!")

        def displayCount(self):
            print("Total Employee %d" % Empleado.empCount)

        def displayEmployee(self):
            print("Name : ", self.name, ", Salary: ", self.salary)


    "creamos el primero objeto"
    emp1 = Empleado("Zara", 2000)
    "creamos un segundo objeto de Empleado"
    emp2 = Empleado("Manni", 5000)

    emp1.displayEmployee()
    emp2.displayEmployee()
    print("Acceso a atributos y métodos estáticos")
    print("Número de Empleados %d" % Empleado.empCount)
    Empleado.presenta("Dato")

    # Como en JS tiene atributos dinámicos
    emp1.age = 7  # Add an 'age' attribute.
    emp1.age = 8  # Modify 'age' attribute.

    print("Edad del empleado:", emp1.age)
    # No funciona porque no tiene ese atributo
    # print("Edad del empleado:",emp2.age)
    del emp1.salary  # Delete 'salary' attribute.
    # No funciona porque no tiene ese atributo
    # print(emp1.salary)
    print(emp2.salary)

    # funciones de control de atributos dinámicos
    hasattr(emp1, 'age')  # Returns true if 'age' attribute exists
    getattr(emp1, 'age')  # Returns value of 'age' attribute
    setattr(emp1, 'age', 8)  # Set attribute 'age' at 8
    print("getattr(emp1, 'age')")
    print(getattr(emp1, 'age'))
    delattr(emp1, 'age')  # Delete attribute 'age'
    print("hasattr(emp1, 'name')")
    print(hasattr(emp1, 'name'))

    print("métodos predefinidos")
    print("Empleado.__doc__:", Empleado.__doc__)
    print("Empleado.__name__:", Empleado.__name__)
    print("Empleado.__module__:", Empleado.__module__)
    print("Empleado.__bases__:", Empleado.__bases__)
    print("Empleado.__dict__:", Empleado.__dict__)


    class Point:
        def __init__(self, x=0, y=0):
            self.x = x
            self.y = y

        def __del__(self):
            class_name = self.__class__.__name__
            print(class_name, "destroyed")


    pt1 = Point()
    pt2 = pt1
    pt2.x = 2
    print(pt1.x)
    pt3 = pt1
    print(id(pt1), id(pt2), id(pt3))  # prints the ids of the objects
    del pt1
    del pt2
    del pt3


    class Point:
        def __init__(self, x=0, y=0):
            self.x = x
            self.y = y

        def __del__(self):
            class_name = self.__class__.__name__
            print(class_name, "destroyed")

        def getX(self):
            return self.x

        def setX(self, x):
            self.x = x


    punto = Point(12, 15)
    print(punto.x)
    print(punto.y)
    punto.setX(10)
    print(punto.x)
    print(punto.getX())

    print("Ejemplos de herencia")
    class Madre:
        def __init__(self, nombre= ""):
            self.nombre = nombre

    class Hija(Madre):
        "hola"

    hija = Hija()
    hija.nombre = "Aroa"
    print(hija.nombre)


    class Madre:
        def __init__(self, nombre= ""):
            self.nombre = nombre

    class Hija(Madre):
        def __init__(self, nombre= "", edad = 0):
            super().__init__(nombre)
            self.edad = edad

    print("Ejemplos de herencia con llamada al init de la madre")
    hija = Hija("Aroa", 37)
    print(hija.nombre)
    print(hija.edad)

    class Madre:  # clase madre
        atributoEstatico = 100

        def __init__(self,atributo_madre = 100):
            print("Constructora de la Madre")
            self.atributoMadre = atributo_madre

        def metodoMadre(self):
            print('Método de la madre')

        def setAttr(self, attr):
            self.atributoMadre = attr

        def getAttr(self):
            return self.atributoMadre

    # Clase Hija hereda de la clase Madre
    class Hija(Madre):  # clase hija
        def __init__(self, atributo_madre = 100, atributo_hija = 10):
            print("Llamando a la constructura de la hija")
            super().__init__(atributo_madre)
            self.atributoHija = atributo_hija

        def metodoHija(self):
            print('llamando al método de la hija')


    print("Datos de herencia")
    Madre.atributoEstatico = 200
    print(Madre.atributoEstatico)
    print(Hija.atributoEstatico)
    c = Hija()  # instancia hija
    c.metodoHija()  # llama al método de la hija
    c.metodoMadre()  # llama a método de la madre
    c.setAttr(200)  # llama al método de la madre
    print(c.getAttr())  # llama al método de la madre
    print(c.atributoHija) # llama al atributo de la hija

    print("Herencia múltiple")
    class A:  # define your class A
        def __init__(self):
            self


    class B:  # define your class B
        def __init__(self):
            self


    class C(A, B):  # subclass of A and B
        def __init__(self):
            self


    class Parent:  # define parent class
        def myMethod(self):
            print('Calling parent method')


    class Child(Parent):  # define child class
        # Redeclara el método para sobreescribirlo
        def myMethod(self):
            print('Calling child method')

    print("llamado a un método sobre escrito de la clase Padre")
    c = Child()  # instance of child
    c.myMethod()  # child calls overridden method


    class Parent:
        def __init__(self):
            self.parameter = ""


    class Child(Parent):
        def __init__(self):
            super(Child, self).__init__()
            self.attribute = ""


    print("Accesos a atributos definidos en clase hija y madre")
    c = Child()
    print(c.parameter)
    print(c.attribute)


    class Parent:
        def __init__(self, parameter=""):
            self.parameter = parameter

        def metodoMadre(self):
            print("Metodo madre en Madre")
        def getParameter(self):
            return self.parameter
        def setParameter(self, parameter):
            self.parameter = parameter


    class Child(Parent):
        def __init__(self, attribute="", parameter=""):
            # self.parameter=parameter
            super().__init__(parameter)
            self.attribute = attribute


        def metodoHija(self):
            self.metodoMadre()

        def metodoMadre(self):
            print("Metodo Madre en Hija")
            super().metodoMadre()


    c = Child("Hola")
    print(c.parameter)
    print("Acceso a métodos getter y setter de clase madre desde hija")
    c.setParameter("Hola")
    print(c.getParameter())
    print(c.attribute)
    c.metodoMadre()
    c.metodoHija()

    c = Child("Hola", "Mundo")
    print(c.parameter)
    print(c.attribute)
    c.metodoMadre()
    c.metodoHija()


    # Definimos la primera clase madre
    class Madre1:
        def __init__(self, nombre):
            self.nombre = nombre

        def saludar(self):
            return f"Hola, mi nombre es {self.nombre}."


    # Definimos la segunda clase madre
    class Madre2:
        def __init__(self, edad):
            self.edad = edad

        def mostrar_edad(self):
            return f"Tengo {self.edad} años."


    # Definimos la clase hija, que hereda de Madre1 y Madre2
    class Hija(Madre1, Madre2):
        def __init__(self, nombre, edad):
            # Llamamos a los constructores de ambas clases padre
            Madre1.__init__(self, nombre)
            Madre2.__init__(self, edad)

        def presentar(self):
            # Utilizamos los métodos de las clases padres
            saludo = self.saludar()
            edad = self.mostrar_edad()
            return f"{saludo} {edad}"

        def __str__(self):
            # Devuelve una representación en cadena de todos los atributos
            return f"Nombre: {self.nombre}, Edad: {self.edad}"

    # Creamos una instancia de la clase hija
    persona = Hija("Ana", 25)
    print(persona.presentar())
    print(persona)


    class Vector:
        def __init__(self, a, b):
            self.a = a
            self.b = b

        def __str__(self):
            return 'Vector (%d, %d)' % (self.a, self.b)

        def __add__(self, other):
            return Vector(self.a + other.a, self.b + other.b)


    v1 = Vector(2, 10)
    v2 = Vector(5, -2)
    print(v1 + v2)


    class JustCounter:
        __secretCount = 0

        def count(self):
            self.__secretCount += 1
            print(self.__secretCount)


    counter = JustCounter()
    counter.count()
    counter.count()


    # print (counter.__secretCount)


    class Cliente:
        ""
        totalCotizaciones = 0

        def __init__(self, nombre="", direccion="", email="", tlf="", cotizacion=0):
            self.nombre = nombre
            self.direccion = direccion
            self.email = email
            self.tlf = tlf
            self.cotizacion = cotizacion

        def setCotizacion(self, cotizacion):
            self.cotizacion = cotizacion
            self.totalCotizaciones += cotizacion

        def __str__(self):
            return "Cliente[nombre:" + self.nombre + "]"


    indra = Cliente()
    indra.setCotizacion(20000)
    print(indra.cotizacion)
    telefonica = Cliente(nombre="Telefónica")
    print(telefonica.nombre)
    print(telefonica.__str__())


    class Figuras(object):
        __slots__ = ('dim1', 'dim2')

        def __init__(self, dim1, dim2):
            self.dim1 = dim1
            self.dim2 = dim2

        def area(self):
            print(('El área de la figura no esta definida.'))


    class Rectangulo(Figuras):
        def __init__(self, dim1, dim2):
            super().__init__(dim1, dim2)

        def area(self):
            if self.dim1 != self.dim2:
                print(('El área del rectángulo es: '))
            else:
                print(('El área del cuadrado es:'))
            return (self.dim1 * self.dim2)

        def perimetro(self):
            print(('El perímetro del rectángulo es: '))
            return (2 * self.dim1 + 2 * self.dim2)


    def main():
        F = Figuras(10, 10)
        R = Rectangulo(9, 5)


    main()


    class Persona:
        def __init__(self, nombre=""):
            self.nombre = nombre


    class Empleado(Persona):
        def __init__(self, nombre="", sueldo=0):
            super().__init__(nombre)
            self.sueldo = sueldo


    perso = Persona("personita")
    emp1 = Empleado(nombre="Juan", sueldo=65000)

    print(perso.nombre)
    print(emp1.sueldo)
    print(emp1.nombre)


    class Persona:
        def __init__(self, nombre="", edad=0):
            self.nombre = nombre
            self.edad = edad
            print("init de Persona")

        def __str__(self):
            return "Persona[ nombre:" + self.nombre + ", edad:" + str(self.edad) + "]"


    class Empleado(Persona):
        def __init__(self, nombre="", edad=0, sueldo=0):
            super().__init__(nombre, edad)
            self.sueldo = sueldo
            print("init de Empleado")


    class Jefe(Empleado):

        def __init__(self, nombre="", edad=0, sueldo=0, departamento=""):
            super().__init__(nombre, edad, sueldo)
            self.departamento = departamento


    perso = Persona("personita")
    emp1 = Empleado(nombre="Juan", sueldo=65000)
    jefecillo = Jefe("Rodrigo", 56, 0, "Carcel")

    print(perso.nombre)
    print(emp1.sueldo)
    print(emp1.nombre)
    print(jefecillo.nombre)
    print(jefecillo.sueldo)
    print(jefecillo.departamento)

    emp2 = Empleado()
