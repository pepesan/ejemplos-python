# -*- coding: utf-8 -*-

# Importing libraries
import pandas as pd
import sys

def main():
    # Print Python and Pandas versions
    print('Python version: ' + sys.version)
    print('Pandas version: ' + pd.__version__)

    # Our small dataset as a dictionary
    d = {'one': [1, 1, 4, 4, 5],
         'two': [2, 2, 3, 3, 5],
         'letter': ['a', 'a', 'b', 'b', 'c']}

    # Create a DataFrame from the dictionary
    df = pd.DataFrame(d)
    print("DataFrame:")
    print(df)

    # Create a group object based on the 'letter' column
    print("Group by 'letter':")
    one = df.groupby('letter')
    print(one)

    # Apply the sum function to the grouped data
    print("Sum of grouped data:")
    print(one.sum())

    # Apply the count function to the grouped data
    print("Count of grouped data:")
    print(one.count())

    # Group by both 'letter' and 'one', and apply the sum function
    print("Group by both 'letter' and 'one' with sum:")
    letterone = df.groupby(['letter', 'one']).sum()
    print(letterone)

    # Display the multi-level index of the grouped result
    print("Index of grouped data:")
    print(letterone.index)

    # Group by both 'letter' and 'one' without setting the grouped columns as index
    letterone = df.groupby(['letter', 'one'], as_index=False).sum()
    print("Group by 'letter' and 'one' with as_index=False:")
    print(letterone)

    # Create a new DataFrame
    df = pd.DataFrame({
        'A': [1, 2, 3, 4, 5],
        'B': [6, 7, 8, 9, 10],
        'C': ['a', 'b', 'c', 'a', 'b']
    })
    print("New DataFrame:")
    print(df)

    # Group by 'C' and calculate the sum of 'A' and 'B'
    print("Group by 'C' and calculate sum of 'A' and 'B':")
    grouped_result = df.groupby('C').agg(sum_A=('A', 'sum'), sum_B=('B', 'sum'))
    print(grouped_result)

# Run the main function if this script is executed
if __name__ == "__main__":
    main()
