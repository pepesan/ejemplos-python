# -*- coding: utf-8 -*-

# Importing pandas
import pandas as pd
import sys

def main():
    # Print Python and Pandas versions
    print('Python version: ' + sys.version)
    print('Pandas version: ' + pd.__version__)

    # Our small dataset (a simple list of integers)
    d = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    # Create a DataFrame
    df = pd.DataFrame(d)
    print(df)
    print("Shape of DataFrame:", df.shape)

    # Rename the column
    df.columns = ['Rev']
    print("DataFrame with renamed column:")
    print(df)

    # Add a new column with a constant value
    df['NewCol'] = 5
    print("DataFrame with new column:")
    print(df)

    # Modify the newly added column
    df['NewCol'] = df['NewCol'] + 1
    print("DataFrame after modifying 'NewCol':")
    print(df)

    # Add another new column based on existing data
    df['NewCol2'] = df['NewCol'] + 1
    print("DataFrame with 'NewCol2':")
    print(df)

    # Delete a column
    del df['NewCol2']
    print("DataFrame after deleting 'NewCol2':")
    print(df)

    # Add a couple more columns
    df['test'] = 3
    df['col'] = df['Rev'] + 1
    print("DataFrame with additional columns:")
    print(df)

    # Change the index of the DataFrame
    i = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
    df.index = i
    print("DataFrame with new index:")
    print(df)

    # Select a single row using .loc
    print("Row 'a' using .loc:")
    print(df.loc['a'])

    # Select a range of rows using .loc (inclusive)
    print("Rows 'a' to 'd' using .loc:")
    print(df.loc['a':'d'])

    # Select a range of rows using .iloc (exclusive)
    print("Rows 0 to 3 using .iloc:")
    print(df.iloc[0:3])

    # Select a single column
    print("Column 'Rev':")
    print(df['Rev'])

    # Select multiple columns
    print("Columns 'Rev' and 'test':")
    print(df[['Rev', 'test']])

    # Select specific rows and a single column
    print("First 3 rows of column 'Rev' using .loc:")
    print(df.loc[df.index[0:3], 'Rev'])

    # Select specific rows and another column
    print("Rows from index 5 to the end for column 'col' using .loc:")
    print(df.loc[df.index[5:], 'col'])

    # Select specific rows and multiple columns
    print("First 3 rows of columns 'col' and 'test' using .loc:")
    print(df.loc[df.index[:3], ['col', 'test']])

    # Display the top N rows of the DataFrame (default is 5)
    print("Top rows of DataFrame:")
    print(df.head())

    # Display the bottom N rows of the DataFrame (default is 5)
    print("Bottom rows of DataFrame:")
    print(df.tail())

# Run the main function if this script is executed
if __name__ == "__main__":
    main()
