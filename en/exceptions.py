if __name__ == '__main__':
    # -*- coding: utf-8 -*-
    """
    Basic Exception Handling
    """
    import sys

    try:
        f = open('myfile.txt')
        s = f.readline()
        i = int(s.strip())
    except OSError as err:
        print("OS error: {0}".format(err))
    except ValueError:
        print("Could not convert data to an integer.")
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise
    """
    Chained Exceptions
    """
    try:
        f = open("test.txt", encoding='utf-8')
        # Perform file operations
    except:
        print("Failed to open the file")
    finally:
        try:
            f.close()
        except Exception as e:
            print("Could not close the file because it was not open")
            print("Error:" + str(e))
    """
    Catching ValueError Exception
    """
    while True:
        try:
            n = input("Please enter an integer: ")
            n = int(n)
            break
        except ValueError:
            print("Not a valid integer! Please try again ...")
        finally:
            print("This will always execute")
    print("Great, you successfully entered an integer!")
    """
    Using Else
    """
    try:
        fh = open("testfile", "w")
        fh.write("This is my test file for exception handling!!")
    except IOError:
        print("Error: can't find file or read data")
    else:
        print("Written content in the file successfully")
        fh.close()
    """
    Using Finally
    """
    try:
        x = float(input("Your number: "))
        print(x)
        inverse = 1.0 / x
    except ValueError:
        print("You should have given either an int or a float")
    except ZeroDivisionError:
        print("Infinity")
    finally:
        print("There may or may not have been an exception.")

    """
    Raising an Exception
    """
    import sys

    try:
        f = open('myfile.txt')
        s = f.readline()
        i = int(s.strip())
    except OSError as err:
        print("OS error: {0}".format(err))
    except ValueError:
        print("Could not convert data to an integer.")
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise
    """
    Accessing Exception Parameters
    """
    try:
        raise Exception('spam', 'eggs')
    except Exception as inst:
        print(type(inst))  # The exception instance
        print(inst.args)  # Arguments stored in .args
        print(inst)  # __str__ allows args to be printed directly,
        # but may be overridden in exception subclasses
        x, y = inst.args  # Unpack args
        print('x =', x)
        print('y =', y)

    """
    Custom Exceptions
    """


    class B(Exception):
        pass


    class C(B):
        pass


    class D(C):
        pass


    for cls in [B, C, D]:
        try:
            raise cls()
        except D:
            print("D")
        except C:
            print("C")
        except B:
            print("B")
    """
    More Complete Custom Exceptions
    """


    class Error(Exception):
        """Base class for exceptions in this module."""
        pass


    class InputError(Error):
        """Exception raised for errors in the input.

        Attributes:
            expression -- input expression in which the error occurred
            message -- explanation of the error
        """

        def __init__(self, expression, message):
            self.expression = expression
            self.message = message


    class TransitionError(Error):
        """Raised when an operation attempts a state transition that's not
        allowed.

        Attributes:
            previous -- state at beginning of transition
            next -- attempted new state
            message -- explanation of why the specific transition is not allowed
        """

        def __init__(self, previous, next, message):
            self.previous = previous
            self.next = next
            self.message = message


    class HTTPException(Exception):

        def __init__(self, message='HTTPERROR: ', status='400', url='telecable.es'):
            self.message = message
            self.status = status
            self.url = url

        def __str__(self):
            return self.message


    try:
        raise HTTPException("HTTPERROR: the server is not responding", '500', "cursosdedesarrollo.com")
    except HTTPException as e:
        print(e)
        print(e.message)
        print(e.url)
        print(e.status)
