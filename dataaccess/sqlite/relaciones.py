"""
Código en proceso
# importación declaraciones
from sqlalchemy.orm import declarative_base

# importar clase Base
Base = declarative_base()
# importaciones de operaciones y campos
from sqlalchemy import Column, Integer, String, select, update



from sqlalchemy.orm import relationship


class Parent(Base):
    __tablename__ = 'parent'
    id = Column(Integer, primary_key=True)
    children = relationship("Child")

class Child(Base):
    __tablename__ = 'child'
    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, ForeignKey('parent.id'))

# importación de la conexión
from sqlalchemy import create_engine
# conexión a memoria sqlite
engine = create_engine('sqlite:///:memory:', echo=True)
# Volcado del esquema en la BBDD
Base.metadata.create_all(engine)
# Manejo de sesiones
from sqlalchemy.orm import sessionmaker

Session = sessionmaker(bind=engine)
# objeto de sesión
session = Session()

parent = Parent()
child = Child()
parent.children
u1 = User(name='pkrabs', fullname='Pearl Krabs')
print(u1.addresses)

a1 = Address(email_address="pearl.krabs@gmail.com")
u1.addresses.append(a1)
print(a1)
print(u1)

session.add(u1)
print(u1)
"""