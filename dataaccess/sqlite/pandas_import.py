import pandas as pd
import sqlite3

con = sqlite3.connect("author_book_publisher.db")

# Load the data into a DataFrame
df = pd.read_sql_query("SELECT * from author", con)

# Select only data for first_name=Paul
new_df = df[df.first_name == "Paul"]

# Write the new DataFrame to a new SQLite table
new_df.to_sql("other_authors", con, if_exists="replace")

con.close()