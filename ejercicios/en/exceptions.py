if __name__ == '__main__':
    import os

    # File handling exception
    filename = "data.txt"

    try:
        with open(filename, "r") as file:
            content = file.read()
            print("File content:", content)
    except FileNotFoundError:
        print(f"{filename} not found. Creating the file with initial content.")
        with open(filename, "w") as file:
            file.write("Initial data")
            file.close()

    # List access exception handling
    my_list = [1, 2, 3, 4, 5]

    try:
        print("Accessing the 10th element:", my_list[9])
    except IndexError as e:
        # Access to the name of the class
        print(e.__class__)
        ## Accesing the error
        print(f"Error: {e}. The list has only {len(my_list)} elements.")


    # Custom exception definition
    class DivisionByZeroError(Exception):
        pass


    # Function to divide with custom exception handling
    def divide(a, b):
        if b == 0:
            raise DivisionByZeroError("Cannot divide by zero!")
        return a / b


    # Using the function
    try:
        result = divide(10, 0)
        print("Result:", result)
    except DivisionByZeroError as e:
        print(f"Custom Error Caught: {e}")
