from ejercicios.clases import Cliente


def test_constructor_without_args():
    """Check .field functionality of namedtuple."""
    c = Cliente()
    assert c.nombre == ""
    assert c.direccion == ""
    assert c.tlf == ""
    assert c.email == ""
    assert c.cotizacion == 0
    assert c.total_cotizaciones == 0


def test_constructor_with_args():
    """Check .field functionality of namedtuple."""
    nombre = "Pepe"
    direccion = "C/ de mmi casa"
    tlf = "923123123"
    email = "pepesan@gmail.com"
    cotizacion = 0
    total_cotizaciones = 0
    c = Cliente(nombre=nombre, direccion=direccion, tlf=tlf, email=email, cotizacion=cotizacion,
                total_cotizaciones=total_cotizaciones)
    assert c.nombre == nombre
    assert c.direccion == direccion
    assert c.tlf == tlf
    assert c.email == email
    assert c.cotizacion == cotizacion
    assert c.total_cotizaciones == total_cotizaciones
