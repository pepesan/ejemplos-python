if __name__ == '__main__':
    # Solution for Exercise 1: Writing and Reading Student Grades
    filename = "student_grades.txt"

    # Step 1: Write to the file
    print("Enter the names and grades of 5 students:")
    with open(filename, 'w') as file:
        for i in range(5):
            student_name = input(f"Enter name of student {i + 1}: ")
            grade = input(f"Enter grade for {student_name}: ")
            file.write(f"{student_name}: {grade}\n")

    # Step 2: Read from the file and display content
    print("\nReading from the file...")
    with open(filename, 'r') as file:
        content = file.readlines()
        for line in content:
            print(line.strip())

    # Solution 2: Writing and counting words without functions
    filename = "user_input.txt"

    # Step 1: Get the sentence from the user and write it to the file
    sentence = input("Enter a sentence: ")
    with open(filename, 'w') as file:
        file.write(sentence)

    # Step 2: Read from the file and count the words
    with open(filename, 'r') as file:
        content = file.read()
        word_count = len(content.split())  # Split the sentence into words using spaces

    print(f"\nThe sentence contains {word_count} words.")
