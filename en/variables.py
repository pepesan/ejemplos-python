# -*- coding: utf-8 -*-

if __name__ == '__main__':
    """
    Statements with variables:

    1- Declaration of a variable (construction) with the variable type
    2- Initialization of a variable (assigning an initial value)
    3- Changing the value of a variable (assigning a new value)
    4- Releasing memory (destroying) a variable
    """

    """
        Declare a variable called counter,
        of integer type, and
        initialize it with the value 100
    """
    counter = 100  # an integer number
    salary = 35000.70  # floating point
    name = "David"  # a string of characters
    boolean_value = False  # a boolean

    print(boolean_value)
    print(salary)
    print(name)
    print(counter)

    # Assignment statement
    # Assigns a new value to a variable
    # Changes the value stored in the variable
    name = "Dog"
    print(name)

    # Python allows dynamic typing
    # A variable can change its type
    salary = "A lot"
    print(salary)

    """
    When a program finishes execution, the memory
    used by declared and in-use variables is released
    """
    print("Examples of variables")
    print("Booleans")
    # Declaration and initialization of a variable
    # boolean: True, False, None
    boolean_var = True
    # Print the variable to the screen
    print(boolean_var)

    print("Integers")
    # Declaration of an integer variable
    # Integers: natural numbers, positive or negative
    integer = 2
    # Print the variable to the screen
    print(integer)
    # Change the value of the variable
    integer = 3
    # Print the variable to the screen
    print(integer)
    # Define another integer variable
    number = 2
    # Print the variable to the screen
    print(number)

    print("Floating-point numbers")
    # Declaration of floating-point variables
    floating_point = +1.23
    # Print the variable to the screen
    print(floating_point)
    # Negative value
    floating_point = -1.23
    # Print the variable to the screen
    print(floating_point)
    # Cast from integer to float
    integer = 2
    floating_point = float(integer)
    # Print the variable to the screen
    print(floating_point)
    # Cast from float to integer
    integer = int(floating_point)
    print(integer)

    # Complex numbers
    print("Complex numbers")
    # Define real and imaginary parts
    complex_number = complex(2, 1)
    print(complex_number)
