if __name__ == '__main__':
    # classes.py

    class Client:
        def __init__(self, name=None, address=None, phone=None, email=None, quotation=None):
            self.name = name
            self.address = address
            self.phone = phone
            self.email = email
            self.quotation = quotation
            self.total_quotations = 0

        def setQuotation(self, quotation):
            self.quotation = quotation
            self.total_quotations += 1


    # Creating an object of the Client class named indra
    indra = Client(name="Indra", address="123 Street", phone="1234567890", email="indra@example.com")

    # Modify the quotation for indra using the setQuotation method
    indra.setQuotation(5000)

    # Verifying the data has been modified correctly
    print("Client Name:", indra.name)
    print("Address:", indra.address)
    print("Phone:", indra.phone)
    print("Email:", indra.email)
    print("Quotation:", indra.quotation)
    print("Total Quotations:", indra.total_quotations)

    # Initializing multiple Client objects with different numbers of parameters
    client1 = Client(name="Alice")
    client2 = Client(name="Bob", address="456 Avenue")
    client3 = Client(name="Charlie", address="789 Road", phone="9876543210", email="charlie@example.com",
                     quotation=10000)

    # Printing to verify data
    print("\nClient 1:", vars(client1))
    print("Client 2:", vars(client2))
    print("Client 3:", vars(client3))

    class Helper:
        count1 = 0
        @classmethod
        def add(cls, a = 0, b = 0):
            return a + b

    ret = Helper.add(1,2)
    print(ret)
    print(Helper.count1)
    Helper.count1 += 1
    print(Helper.count1)



















