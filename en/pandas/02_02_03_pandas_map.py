import pandas as pd
import numpy as np

def main():
    # Create a DataFrame
    df = pd.DataFrame([[1, 2.12], [3.356, 4.567], [4.356, 5.567]])
    print("Original DataFrame:")
    print(df)
    print("Shape of DataFrame:", df.shape)

    # Rename columns
    columnas_a_transformar = ['columna1', 'columna2']
    df.columns = columnas_a_transformar

    # Apply a lambda function to all elements of the DataFrame
    # Apply an operation to all elements in the DataFrame using vectorized operations
    df2 = df - 1
    print("DataFrame after subtracting 1 from all elements:")
    print(df2)
    print("Shape of transformed DataFrame:", df2.shape)

    # Apply a lambda function to a single column
    df3 = df['columna1'].map(lambda x: x - 1)
    print("Transformed column 'columna1':")
    print(df3)
    print("Shape of transformed column:", df3.shape)

    # Define a named function for transformation
    def resta_uno(x):
        if pd.isna(x):
            return np.nan  # Handle missing values appropriately
        else:
            return x - 1

    # Apply the named function to a column
    df4 = df['columna1'].map(resta_uno)
    print("Column 'columna1' after applying named function:")
    print(df4)
    print("Shape of transformed column:", df4.shape)

    # Filter the DataFrame based on a condition
    df_filtrado = df[df['columna1'] > 1]
    print("Filtered DataFrame where 'columna1' > 1:")
    print(df_filtrado)

    # Define cutoff values
    valor_corte_columna1 = 2
    valor_corte_columna2 = 5

    # Filter the DataFrame based on multiple conditions
    df_filtrado = df[(df['columna1'] > valor_corte_columna1) & (df['columna2'] > valor_corte_columna2)]
    print("Filtered DataFrame with multiple conditions:")
    print(df_filtrado)

    # Query rows with specific conditions
    print("Rows where 'columna2' > 5:")
    print(df.query('columna2 > 5'))

    # Create two DataFrames for concatenation
    df1 = pd.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6]})
    df2 = pd.DataFrame({'C': [7, 8, 9], 'B': [10, 11, 12]})

    # Concatenate DataFrames vertically
    print("Concatenated DataFrame:")
    print(pd.concat([df1, df2]))

# Run the main function if this script is executed
if __name__ == "__main__":
    main()
