# -*- coding: utf-8 -*-
if __name__ == "__main__":
    midict = {
        'Name': 'Zara',
        'Age': 7,
        'Class': 'First',
        0 : (1,2,3),
        1 : [1,2,3],
        2 : {
            0: 0
        },
    }
    print(midict)
    print("dict['Name']: ", midict['Name'])
    print("dict['Age']: ", midict['Age'])


    midict['Age'] = 8  # update existing entry
    midict['School'] = "DPS School"  # Add new entry
    midict['Last Name'] = "Petterson"

    print(midict)
    # devuelve las claves
    print(midict.keys())
    # devuelve los valores
    print(midict.values())
    print(midict['Last Name'])

    for clave in midict:
        print(clave)
        print(midict[clave])


    midict['miarray'] = [1, 2]
    print("eliminando una clave")
    del midict['Name']  # remove entry with key 'Name'
    print(midict)
    midict.clear()     # remove all entries in dict
    print(midict)
    del midict       # delete entire dictionary


    midict = {'Name': 'Zara', 'Age': 7}
    print(len(midict))

    midict['Apellidos'] = {
        'primero': "Luces",
        'segundo': "Apagadas"
    }

    print(midict)
    print(midict['Apellidos']['primero'])
