if __name__ == '__main__':
    # -*- coding: utf-8 -*-

    # Writing to a file
    string = "my text"
    fobj_out = open("ad_lesbiam.txt", "w")
    fobj_out.write(string)
    fobj_out.close()

    fobj_in = open("ad_lesbiam.txt")
    fobj_out = open("ad_lesbiam2.txt", "w")
    i = 1
    for line in fobj_in:
        print(line.rstrip())
        fobj_out.write(str(i) + ": " + line)
        i = i + 1
    fobj_in.close()
    fobj_out.close()

    # As a list
    lines = open("ad_lesbiam.txt").readlines()
    print(lines)

    # As a string
    text = open("ad_lesbiam.txt").read()
    print(text[16:34])

    # Reading a CSV file
    # Open the file in read mode
    with open('./file.csv', 'r') as file:
        # Read all lines from the file
        lines = file.readlines()
        print(lines)

    # Process the content
    data = []
    for line in lines:
        # Remove line breaks and split values by commas
        row = line.strip().split(',')
        data.append(row)

    # Print the content
    for row in data:
        print(row)


    # Example of writing and reading binary files without using functions

    # List of integers to store in the binary file
    numbers = [10, 20, 30, 40, 50]

    # ----- Writing to a binary file -----
    # Open the file in binary write mode ('wb')
    file_out = open("numbers.bin", "wb")

    # Write each integer to the file as binary data
    for number in numbers:
        # Convert the integer to bytes (4 bytes for each integer) and write to the file
        file_out.write(number.to_bytes(4, byteorder='little', signed=True))

    # Close the file after writing
    file_out.close()

    # ----- Reading from the binary file -----
    # Open the file in binary read mode ('rb')
    file_in = open("numbers.bin", "rb")

    # Initialize an empty list to store the integers read from the file
    read_numbers = []

    # Read the file 4 bytes at a time (size of an integer in bytes)
    while True:
        # Read 4 bytes from the file
        data = file_in.read(4)
        if not data:
            # Break the loop when there is no more data to read
            break
        # Convert the bytes back to an integer and append to the list
        number = int.from_bytes(data, byteorder='little', signed=True)
        read_numbers.append(number)

    # Close the file after reading
    file_in.close()

    # Print the list of integers read from the file
    print("List of integers read from the binary file:", read_numbers)


