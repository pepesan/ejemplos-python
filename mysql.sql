CREATE TABLE users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL
);

INSERT INTO users (email, password) VALUES
('user1@example.com', 'hashed_password_1'),
('user2@example.com', 'hashed_password_2'),
('user3@example.com', 'hashed_password_3');