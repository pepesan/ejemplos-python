from ejercicios.funciones_prueba import suma


def test_suma_simple():
    n1 = 2
    n2 = 1
    resEsperado = 3
    res = suma(n1, n2)
    assert res == resEsperado
