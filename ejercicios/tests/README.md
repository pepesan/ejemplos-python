# Lanzamiento de pruebas

##Instalación
pip install pytest

##Lanzamiento
Desde la carpeta base de ejercicios ejecutar
<code>pytest</code>
### Salida
<code>
pepesan@sauron:~/PycharmProjects/ejemplos-python-v4/ejercicios$ pytest
====================================================================== test session starts =======================================================================
platform linux -- Python 3.9.7, pytest-6.2.5, py-1.11.0, pluggy-1.0.0
rootdir: /home/pepesan/PycharmProjects/ejemplos-python-v4/ejercicios
collected 4 items                                                                                                                                                

tests/test_clases_01.py ..                                                                                                                                 [ 50%]
tests/test_funciones_p1.py .                                                                                                                               [ 75%]
tests/test_funciones_p2.py .                                                                                                                               [100%]

======================================================================= 4 passed in 0.02s ========================================================================
</code>