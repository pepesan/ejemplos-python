if __name__ == '__main__':
    # -*- coding: utf-8 -*-

    # Function without parameters
    def procedure():
        print("procedure")

    # Call
    procedure()

    # function definition
    def sum(x, y):
        """returns x + y"""
        # define some more sentences
        return x + y

    # call to the function returning a value
    res = sum(2, 3)
    print(res)


    # Function to calculate the sum of two numbers
    def addition(a, b):
        """Returns a + b"""
        print("a:", a)
        print("b:", b)
        return a + b


    # Variable to store results
    result = addition(4, 7)
    print(result)

    # result = 5
    result = addition(2, 3)
    print("Result: ", result)

    result = addition(4, 9)
    print(result)

    result = addition(2, b=3)
    print(result)

    result = addition(a=2, b=3)
    print(result)

    result = addition(b=3, a=2)
    print(result)


    # Function to subtract two numbers with a default value for the second parameter
    def subtraction(a, b=1):
        """Optional parameter"""
        return a - b


    # a=5, b=1
    result = subtraction(5)
    print(result)
    result = subtraction(5, 2)
    print(result)
    result = subtraction(b=5, a=2)
    print(result)


    # Function to subtract three numbers with default values
    def subtract_three(a=0, b=0, c=0):
        return a - b - c


    # a=0, b=0, c=0
    print(subtract_three())
    # a=2, b=0, c=0
    print(subtract_three(2))
    # a=2, b=3, c=0
    print(subtract_three(2, 3))
    # a=2, b=3, c=4
    print(subtract_three(2, 3, 4))
    # b=2
    print(subtract_three(b=2))
    # c=2
    print(subtract_three(c=2))


    # Function to multiply two numbers with default values
    def multiply(a=2, b=3):
        return a * b


    # a=2 , b=3
    result = multiply()
    print(result)
    # a=4 , b=3
    result = multiply(4)
    print(result)
    # a=4 , b=5
    result = multiply(4, 5)
    print(result)

    # a=3 , b=5
    result = multiply(3, 5)
    print(result)

    # a=2 , b=3
    result = multiply(b=3)
    print(result)


    # Function a with default value for parameter c
    def a(c=0):
        return 100 + c


    # Function b with default value for parameter n that calls function a
    def b(n=a(20)):
        return n + 50


    # Prints 70
    print(b(20))

    # Prints 170
    print(b())


    # Function with variable number of arguments
    def variable_args(*args):
        return args


    print("Variable number of parameters", variable_args(1, 5, True, False, "Hello, world!"))

    print("Variable number of parameters", variable_args(1, 5))


    # Function with variable number of arguments and keyword arguments
    def variable_args_kwargs(*args, **kwargs):
        return args, kwargs


    print("args + kwargs")
    args, kwargs = variable_args_kwargs(True, False, 3.5, message="Hello, world!", year=2014)
    print(args)
    # (True, False, 3.5)
    print(kwargs)


    # {'message': 'Hello, world!', 'year': 2014}

    # Recursive function to calculate factorial
    def factorial(n):
        if n == 1:
            return 1
        else:
            return n * factorial(n - 1)


    print(factorial(2))


    # Recursive function to calculate factorial with intermediate results
    def factorial_verbose(n):
        print("factorial has been called with n = " + str(n))
        if n == 1:
            return 1
        else:
            result = n * factorial_verbose(n - 1)
            print("Intermediate result for ", n, " * factorial(", n - 1, "): ", result)
            return result


    print(factorial_verbose(5))


    # Function to add 2 to a number
    def add_two(x):
        x = x + 2
        return x


    result = add_two(3)
    print("Result:", result)
    number = 3
    result = add_two(number)
    print("Number:", number)
    print("Result:", result)


    # Function to subtract 2 from a number
    def subtract_two(x):
        x = x - 2
        return x


    result = subtract_two(3)
    print("Result:", result)

    print("Subtraction:", subtract_two(4))

    print("Subtraction:", subtract_two(7))
