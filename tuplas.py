# -*- coding: utf-8 -*-

if __name__ == "__main__":
    tup1 = ()
    print(tup1)
    tup1 = (50,)
    print(tup1)
    print(len(tup1))
    print(tup1[0])
    # falla porque hay un solo valor
    # print(tup1[1])
    tup1 = ('physics', 'chemistry', 1997, 2000)
    tup2 = (1, 2, 3, 4, 5)
    tup3 = "a", "b", "c", "d"
    print("tup3")
    print(tup3)

    tup1 = ('physics', 'chemistry', 1997, 2000)
    tup2 = (1, 2, 3, 4, 5, 6, 7)
    print("tup1[0]")
    print(tup1[0]) # 'physics'
    print("tup1[3]")
    print(tup1[3]) # 2000

    # saldría un fallo con index invalido en tupla
    # print(tup1[4])

    # Coge sólo 4 elementos, empezando por el segundo
    # desde la posición 1 inclusive, hasta la 5 exclusive
    print(tup2[1:5])
    # (2, 3, 4, 5)
    tup1 = (12, 34.56, True)
    tup2 = ('abc', 'xyz')

    # No es posible cambiar el valor dentro de una tupla
    # con una sentencia de asignación
    # tup1[0] = 100; <- Esto no se puede hacer

    numero = 2
    numero = tup1[0]
    numero = tup1[2]
    numero = tup2[0]
    print(numero)

    tup1 = (12, 34.56, True)
    tup2 = ('abc', 'xyz')
    tup3 = tup1 + tup2
    print("tup3")
    print(tup3)
    # (12, 34.56, True, 'abc', 'xyz')
    print(tup3[(0+1)])
    tup = ('physics', 'chemistry', 1997, 2000)

    print (tup)
    del tup
    print ("After deleting tup : ")
    # print (tup)
    # del ha borrado la variable entera

    for item in tup1:
        print(item)