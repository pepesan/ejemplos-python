# importación declaraciones
from sqlalchemy.orm import declarative_base

# importar clase Base
Base = declarative_base()
# importaciones de operaciones y campos
from sqlalchemy import Column, Integer, String, select, update


# declación de la clase/tabla User
class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    fullname = Column(String)
    nickname = Column(String)

    def __repr__(self):
        return "<User(name='%s', fullname='%s', nickname='%s')>" % (self.name, self.fullname, self.nickname)


# importación de la conexión
from sqlalchemy import create_engine

# conexión a memoria sqlite
engine = create_engine('sqlite:///:memory:', echo=True)
# Volcado del esquema en la BBDD
Base.metadata.create_all(engine)

# Objeto entidad
ed_user = User(name='ed', fullname='Ed Jones', nickname='edsnickname')

# Manejo de sesiones
from sqlalchemy.orm import sessionmaker

Session = sessionmaker(bind=engine)
# objeto de sesión
session = Session()

# insertar usuario
session.add(ed_user)
session.flush()

# consulta con filtro
our_user = session.query(User).filter_by(name='ed').first()
print(our_user)

# insert múltiple
session.add_all([
    User(name='wendy', fullname='Wendy Williams', nickname='windy'),
    User(name='mary', fullname='Mary Contrary', nickname='mary'),
    User(name='fred', fullname='Fred Flintstone', nickname='freddy')])

# pendiente en sesión
print(session.new)
# flush de sesión
session.flush()
print(session.new)

# Consulta por ID
some_user = session.get(User, 4)
print(some_user)

# update

session.execute(
    update(User).
        where(User.name == "windy").
        values(fullname="Sandy Squirrel Extraordinaire")
)
session.flush()

# select con first
row = session.execute(select(User)).first()
print(row)
# selección de campos
row = session.execute(select(User.name, User.fullname)).first()
print(row)
# condiciones where
row = session.execute(
    select(User.name, User.fullname).where(User.name == "windy")
).all()
print(row)
# ordenado de resultados
row = session.execute(
    select(User).order_by(User.fullname.desc())
).all()
print(row)

# borrado
mary = session.get(User, 3)
session.delete(mary)
session.flush()
row = session.execute(select(User)).all()
print(row)

# limit y offset
row = session.execute(select(User).limit(2).offset(0)).all()
print(row)

# Agregando un usuario
mary = User(name='mary', fullname='Mary', nickname='mery')
session.add(mary)
# Haciendo rollback
session.rollback()
# Haciendo el commit
session.commit()
session.flush()
row = session.execute(select(User)).all()
print(row)

# Cerrado de sesión
session.close()
