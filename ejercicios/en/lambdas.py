if __name__ == '__main__':

    # Lambda Function
    product = lambda x, y: x * y
    print(product(4, 5))  # Output: 20

    # Map Function
    numbers = [1, 2, 3, 4]
    doubled = map(lambda x: x * 2, numbers)
    print(list(doubled))  # Output: [2, 4, 6, 8]

    # Filter Function
    numbers = [1, 2, 3, 4, 5]
    odds = filter(lambda x: x % 2 != 0, numbers)
    print(list(odds))  # Output: [1, 3, 5]

    # Reduce Function
    from functools import reduce

    numbers = [1, 2, 3, 4]
    product = reduce(lambda x, y: x * y, numbers)
    print(product)  # Output: 24

    # process a list of string to get the string capitalize
    string_list = ["hi", "byebye"]
    map_result = map(lambda s: s.upper(), string_list)
    print(list(map_result))

    # using several at once
    """numbers = [1, 2, 3, 4]
    doubled = map(lambda x: x * 2, numbers)
    odds = filter(lambda x: x % 2 != 0, list(doubled))
    print(list(odds))
    """
    # Decorator
    def my_decorator(func):
        def wrapper():
            print("Start")
            func()
            print("End")

        return wrapper


    @my_decorator
    def greet():
        print("Hello, World!")


    greet()
    # Output:
    # Start
    # Hello, World!
    # End

