if __name__ == '__main__':
    # Importing the class and function from mymodule.py
    from folder.mymodule import Person, is_adult

    # Create an instance of the Person class
    person = Person("Alice", 20)

    # Call the greet method of the Person instance
    person.greet()

    # Check if the person is an adult using the is_adult function
    adult_status = is_adult(person.age)

    # Print the result
    print(f"Is {person.name} an adult? {adult_status}")