if __name__ == '__main__':
    class ClassName:
        """Optional class documentation string"""
        # class_suite


    obj = ClassName()


    class Employee:
        """Class that manages employee data"""

        # comment
        def __init__(self):
            self.name = ""
            self.salary = 0


    jorge = Employee()
    print(jorge.name)
    jorge.name = "Jorge"
    print(jorge.name)
    print(jorge.salary)
    jorge.salary = 45000
    print(jorge.salary)

    juan = Employee()
    juan.name = "Juan"
    juan.salary = 15000
    print(juan.name)
    print(juan.salary)


    class Employee:
        """Class that manages employee data"""

        # comment
        def __init__(self, name="", salary=0):
            self.name = name
            self.salary = salary


    isabel = Employee()
    print(isabel.salary)
    isabel = Employee("Isabel")
    print(isabel.name)
    print(isabel.salary)
    isabel = Employee("Isabel", 60000)
    print(isabel.name)
    print(isabel.salary)
    isabel = Employee(salary=60000)
    print(isabel.salary)
    isabel = Employee(name="Isabel")
    print(isabel.name)


    class Employee:
        """Class that manages employee data"""

        # comment
        def __init__(self, name="", salary=0):
            self.name = name
            self.salary = salary

        def increaseSalary(self, value):
            self.salary += value


    maria = Employee(name="Maria", salary=75000)
    maria.increaseSalary(2000)
    print(maria.salary)


    # maria.salary = maria.salary + 10000
    def increaseSalary(salary, increment):
        return salary + increment


    maria.salary = increaseSalary(maria.salary, 10000)

    maria.increaseSalary(10000)
    print(maria.salary)


    class Employee:
        """Class that manages employee data"""

        # comment
        def __init__(self, name="", salary=0):
            self.name = name
            self.__salary = salary

        def setSalary(self, salary):
            self.__salary = salary

        def getSalary(self):
            return self.__salary


    maria = Employee(name="Maria", salary=75000)
    # Access to private attribute (restricted)
    # print(maria.__salary)
    print(maria.getSalary())
    maria.setSalary(85000)
    print(maria.getSalary())


    class Employee:
        """Class that manages employee data"""

        # comment
        def __init__(self, name="", salary=0):
            self.__name = name
            self.__salary = salary

        def increaseSalary(self, value):
            self.__salary += value

        def setSalary(self, salary):
            self.__salary = salary

        def getSalary(self):
            return self.__salary

        def setName(self, name):
            self.__name = name

        def getName(self):
            return self.__name

        def __str__(self):
            return f'Employee [name: {self.__name}, salary: {self.__salary}]'


    emp1 = Employee("Zara", 2000)
    # emp1.__salary = 30000
    emp1.setSalary(35000)
    print(emp1.getSalary())
    print("__str__ == toString()")
    print(emp1)


    class Employee:
        """Base class for managing employees"""
        empCount = 0  # Static variable shared among all instances

        def __init__(self, name, salary):
            self.name = name
            self.salary = salary
            Employee.empCount += 1

        @classmethod
        def introduce(cls, data):
            print("Hello, my name is", data, "!")

        def displayCount(self):
            print("Total Employee %d" % Employee.empCount)

        def displayEmployee(self):
            print("Name :", self.name, ", Salary: ", self.salary)


    # Creating the first object
    emp1 = Employee("Zara", 2000)
    # Creating a second Employee object
    emp2 = Employee("Manni", 5000)

    emp1.displayEmployee()
    emp2.displayEmployee()
    print("Access to static attributes and methods")
    print("Number of Employees %d" % Employee.empCount)
    Employee.introduce("Data")

    emp1.age = 7  # Add an 'age' attribute
    emp1.age = 8  # Modify 'age' attribute

    print("Employee age:", emp1.age)
    # print("Employee age:", emp2.age)  # Error, attribute does not exist
    # del emp1.salary  # Delete 'salary' attribute
    print(emp2.salary)

    # dynamic attribute control functions
    hasattr(emp1, 'age')  # Returns true if 'age' attribute exists
    getattr(emp1, 'age')  # Returns value of 'age' attribute
    setattr(emp1, 'age', 8)  # Set attribute 'age' to 8
    print("getattr(emp1, 'age')")
    print(getattr(emp1, 'age'))
    delattr(emp1, 'age')  # Delete attribute 'age'
    print("hasattr(emp1, 'name')")
    print(hasattr(emp1, 'name'))

    print("predefined methods")
    print("Employee.__doc__:", Employee.__doc__)
    print("Employee.__name__:", Employee.__name__)
    print("Employee.__module__:", Employee.__module__)
    print("Employee.__bases__:", Employee.__bases__)
    print("Employee.__dict__:", Employee.__dict__)


    class Point:
        def __init__(self, x=0, y=0):
            self.x = x
            self.y = y

        def __del__(self):
            class_name = self.__class__.__name__
            print(class_name, "destroyed")


    pt1 = Point()
    pt2 = pt1
    pt2.x = 2
    print(pt1.x)
    pt3 = pt1
    print(id(pt1), id(pt2), id(pt3))  # prints the ids of the objects
    del pt1
    del pt2
    del pt3


    class Point:
        def __init__(self, x=0, y=0):
            self.x = x
            self.y = y

        def __del__(self):
            class_name = self.__class__.__name__
            print(class_name, "destroyed")

        def getX(self):
            return self.x

        def setX(self, x):
            self.x = x


    point = Point(12, 15)
    print(point.x)
    print(point.y)
    point.setX(10)
    print(point.x)
    print(point.getX())

    class Mother:
        def __init__(self, name=""):
            self.name = name
            print("Calling mother's constructor")

    class Daughter(Mother):  # daughter class inherits from mother
        def __init__(self, age = 0):
            super().__init__()
            self.age = age
            print("Calling daughter's constructor")

    erika = Daughter()
    print(erika.name)

    class Mother:  # mother class
        staticAttribute = 100

        def __init__(self):
            print("Mother's constructor")
            self.motherAttribute = 100

        def motherMethod(self):
            print('Method of the mother')

        def setAttr(self, attr):
            self.motherAttribute = attr

        def getAttr(self):
            return self.motherAttribute


    class Daughter(Mother):  # daughter class inherits from mother
        def __init__(self):
            super().__init__()
            print("Calling daughter's constructor")

        def daughterMethod(self):
            print('Calling daughter method')


    print("Inheritance Data")
    Mother.staticAttribute = 200
    print(Mother.staticAttribute)
    print(Daughter.staticAttribute)
    d = Daughter()  # daughter instance
    d.daughterMethod()  # calls daughter's method
    d.motherMethod()  # calls mother's method
    d.setAttr(200)  # calls mother's method
    print(d.getAttr())  # calls mother's method

    print("Multiple Inheritance")


    class A:  # define class A
        def __init__(self):
            self.name = ""


    class B:  # define class B
        def __init__(self):
            self.surname = ""


    class C(A, B):  # subclass of A and B
        def __init__(self):
            self



    class Parent:  # define parent class
        def myMethod(self):
            print('Calling parent method')


    class Child(Parent):  # define child class
        def myMethod(self):
            print('Calling child method')


    print("Calling overridden method from Parent class")
    c = Child()  # child instance
    c.myMethod()  # child calls overridden method


    class Parent:
        def __init__(self):
            self.parameter = ""


    class Child(Parent):
        def __init__(self):
            super(Child, self).__init__()
            self.attribute = ""


    print("Accessing attributes from child and parent classes")
    c = Child()
    print(c.parameter)
    print(c.attribute)


    class Parent:
        def __init__(self, parameter=""):
            self.parameter = parameter

        def parentMethod(self):
            print("Mother method in Parent")

        def getParameter(self):
            return self.parameter

        def setParameter(self, parameter):
            self.parameter = parameter


    class Child(Parent):
        def __init__(self, attribute="", parameter=""):
            super().__init__(parameter)
            self.attribute = attribute

        def daughterMethod(self):
            self.motherMethod()

        def parentMethod(self):
            print("Mother Method in Child")
            super(Child, self).motherMethod()


    c = Child("Hello")
    print(c.parameter)
    print("Accessing getter and setter methods from parent class in child")
    c.setParameter("Hello")
    print(c.getParameter())
    print(c.attribute)
    c.parentMethod()
    c.daughterMethod()

    c = Child("Hello", "World")
    print(c.parameter)
    print(c.attribute)
    c.motherMethod()
    c.daughterMethod()


    # Define the first parent class
    class Mother1:
        def __init__(self, name):
            self.name = name

        def greet(self):
            return f"Hello, my name is {self.name}."


    # Define the second parent class
    class Mother2:
        def __init__(self, age):
            self.age = age

        def show_age(self):
            return f"I am {self.age} years old."


    # Define the daughter class, inheriting from Mother1 and Mother2
    class Daughter(Mother1, Mother2):
        def __init__(self, name, age):
            # Call constructors of both parent classes
            Mother1.__init__(self, name)
            Mother2.__init__(self, age)

        def introduce(self):
            # Use methods from parent classes
            greeting = self.greet()
            age = self.show_age()
            return f"{greeting} {age}"

        def __str__(self):
            # Return a string representation of all attributes
            return f"Name: {self.name}, Age: {self.age}"


    # Create an instance of the daughter class
    person = Daughter("Anna", 25)
    print(person.introduce())
    print(person)


    class Vector:
        def __init__(self, a, b):
            self.a = a
            self.b = b

        def __str__(self):
            return 'Vector (%d, %d)' % (self.a, self.b)

        def __add__(self, other):
            return Vector(self.a + other.a, self.b + other.b)


    v1 = Vector(2, 10)
    v2 = Vector(5, -2)
    print(v1 + v2)


    class JustCounter:
        __secretCount = 0

        def count(self):
            self.__secretCount += 1
            print(self.__secretCount)


    counter = JustCounter()
    counter.count()
    counter.count()


    class Customer:
        totalQuotes = 0

        def __init__(self, name="", address="", email="", phone="", quote=0):
            self.name = name
            self.address = address
            self.email = email
            self.phone = phone
            self.quote = quote

        def setQuote(self, quote):
            self.quote = quote
            self.totalQuotes += quote

        def __str__(self):
            return "Customer[name:" + self.name + "]"


    indra = Customer()
    indra.setQuote(20000)
    print(indra.quote)
    telefonica = Customer(name="Telefonica")
    print(telefonica.name)
    print(telefonica.__str__())


    class Shapes(object):
        __slots__ = ('dim1', 'dim2')

        def __init__(self, dim1, dim2):
            self.dim1 = dim1
            self.dim2 = dim2

        def area(self):
            print(('The area of the shape is not defined.'))


    class Rectangle(Shapes):
        def __init__(self, dim1, dim2):
            super().__init__(dim1, dim2)

        def area(self):
            if self.dim1 != self.dim2:
                print(('The area of the rectangle is: '))
            else:
                print(('The area of the square is:'))
            return self.dim1 * self.dim2

        def perimeter(self):
            print(('The perimeter of the rectangle is: '))
            return 2 * self.dim1 + 2 * self.dim2


    def main():
        S = Shapes(10, 10)
        R = Rectangle(9, 5)


    main()


    class Person:
        def __init__(self, name=""):
            self.name = name


    class Employee(Person):
        def __init__(self, name="", salary=0):
            super().__init__(name)
            self.salary = salary


    person = Person("someone")
    emp1 = Employee(name="John", salary=65000)

    print(person.name)
    print(emp1.salary)
    print(emp1.name)


    class Person:
        def __init__(self, name="", age=0):
            self.name = name
            self.age = age
            print("init of Person")

        def __str__(self):
            return "Person[ name:" + self.name + ", age:" + str(self.age) + "]"


    class Employee(Person):
        def __init__(self, name="", age=0, salary=0):
            super().__init__(name, age)
            self.salary = salary
            print("init of Employee")


    class Boss(Employee):
        def __init__(self, name="", age=0, salary=0, department=""):
            super().__init__(name, age, salary)
            self.department = department


    person = Person("someone")
    emp1 = Employee(name="John", salary=65000)
    boss = Boss("Rodrigo", 56, 0, "Prison")

    print(person.name)
    print(emp1.salary)
    print(emp1.name)
    print(boss.name)
    print(boss.salary)
    print(boss.department)

    emp2 = Employee()

