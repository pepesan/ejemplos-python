if __name__ == '__main__':
    # -*- coding: utf-8 -*-

    # Importing pandas library
    import pandas as pd

    print(pd.__version__)

    # The initial set of baby names and birth rates
    names = ['Bob', 'Jessica', 'Mary', 'John', 'Mel']
    births = [968, 155, 77, 578, 973]

    # Creating the dataset by zipping names and births
    BabyDataSet = list(zip(names, births))
    print("DataSet")
    print(BabyDataSet)
    # Example output: [('Bob', 968), ('Jessica', 155), ('Mary', 77), ('John', 578), ('Mel', 973)]

    # Creating a DataFrame without column names
    df = pd.DataFrame(data=BabyDataSet)
    print("DataFrame without column names")
    print(df)

    # Creating a DataFrame with specified column names
    df = pd.DataFrame(data=BabyDataSet, columns=['Names', 'Births'])
    print("DataFrame")
    print(df)

    # Display the shape of the DataFrame
    print("DataFrame Shape")
    print(df.shape)

    # Display basic statistics of the DataFrame
    print("DataFrame Describe")
    print(df.describe())

    # Display the shape of the description output
    print("DataFrame Describe Shape")
    print(df.describe().shape)

    # Saving the DataFrame to a CSV file
    Location = './births1880.csv'
    df.to_csv(Location, index=False, header=True, sep=";")
    print(df)

    # Reading the data back from the CSV file
    # Uncomment the following to load the file with different options:
    # df = pd.read_csv(Location, header=False)
    x = pd.read_csv(Location)
    # df = pd.read_csv(Location, names=['Names', 'Births'])
    # df = pd.read_csv(Location, names=['Names', 'Births'])

    print("Data loaded from CSV")
    print(df)

    # Display the first few rows of the DataFrame
    print("First few rows")
    print(df.head())

    # Display the last few rows of the DataFrame
    print("Last few rows")
    print(df.tail())

    # Sorting the DataFrame by the 'Births' column in descending order
    Sorted = df.sort_values(by=['Births'], ascending=False)
    print("First row after sorting by 'Births'")
    print(Sorted)
    print(Sorted.head(2))  # Displaying the top 2 rows
    print(Sorted.tail(2))  # Displaying the bottom 2 rows

    # Selecting a specific column
    print("Column selection")
    print(df['Births'])

    # Display the shape of the selected column
    print("Shape of selected column")
    print(df['Births'].shape)

    # Display the maximum value in the 'Births' column
    print("Maximum value in 'Births'")
    print(df['Births'].max())

    # Creating and displaying a pandas Series
    s = pd.Series([1, 2, 3, 4])
    print("Series")
    print(s)
    print("Shape")
    print(s.shape)

    # Basic statistics for the Series
    print("Statistics for Series")
    print(s.describe())

    # Loading a dictionary into a DataFrame
    print("Loading dictionary into DataFrame")
    d = {'col1': [1, 2], 'col2': [3, 4], 'col3': [5, 6]}
    df = pd.DataFrame(data=d)
    print("DataFrame")
    print(df)
    print("Shape of DataFrame")
    print(df.shape)

    # Display basic statistics of the new DataFrame
    print("Statistics for DataFrame")
    print(df.describe())

    # Loading a DataFrame from the 'Bunch' object in sklearn
    import numpy as np
    from sklearn.datasets import load_iris

    print("Loading iris dataset")

    # Load the iris dataset
    iris = load_iris()

    # Display the type of the dataset and its attributes
    print(type(iris))  # Type of the dataset
    print(type(iris.data))  # Type of the data
    print(type(iris.target))  # Type of the target labels

    # Concatenate the iris data and target arrays, and create a DataFrame
    print("Iris data type")
    print(type(iris['data']))
    print("Iris target type")
    print(type(iris['target']))
    print("Feature names in iris dataset")
    print(iris['feature_names'])

    # Create a DataFrame with iris data and target, including column names
    data1 = pd.DataFrame(data=np.c_[iris['data'], iris['target']],
                         columns=iris['feature_names'] + ['target'])

    # Display the DataFrame information
    print("DataFrame type")
    print(type(data1))
    print("Feature names")
    print(iris['feature_names'])
    print("Target labels")
    print(iris['target'])
    print("Shape of target labels")
    print(iris['target'].shape)
    print("Iris DataFrame")
    print(data1)
