if __name__ == '__main__':
    suma = lambda a, b: a + b
    print(suma(3, 5))  # Salida: 8

    numeros = [1, 2, 3, 4]
    dobles = map(lambda x: x * 2, numeros)  # Multiplica cada número por 2
    print(list(dobles))  # Salida: [2, 4, 6, 8]

    numeros = [1, 2, 3, 4]
    pares = filter(lambda x: x % 2 == 0, numeros)  # Solo números pares
    print(list(pares))  # Salida: [2, 4]

    from functools import reduce

    numeros = [1, 2, 3, 4]
    suma = reduce(lambda x, y: x + y, numeros)  # Suma todos los números
    print(suma)  # Salida: 10


    def aplicar_operacion(func, a, b):
        return func(a, b)


    # Función que suma dos números
    resultado = aplicar_operacion(lambda x, y: x + y, 3, 5)
    print(resultado)  # Salida: 8


    def crear_multiplicador(n):
        return lambda x: x * n


    multiplicar_por_3 = crear_multiplicador(3)
    print(multiplicar_por_3(5))  # Salida: 15


