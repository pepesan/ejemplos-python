if __name__ == '__main__':
    # -*- coding: utf-8 -*-

    # Importing pandas library
    import pandas as pd

    print(pd.__version__)

    data = pd.read_csv('clean.csv')
    print(data.head())  # Displays the first 5 rows of the DataFrame

    # Save the DataFrame to a file
    output_file = 'output.csv'
    data.to_csv(output_file, index=False)
    print(f"Data saved to {output_file}")

    data = {
        'Name': ['Alice', 'Bob', 'Charlie'],
        'Age': [25, 30, 35],
        'City': ['New York', 'Los Angeles', 'Chicago']
    }

    df = pd.DataFrame(data)
    print(df)

    # Access a column
    print(df['Name'])

    # Access multiple columns
    print(df[['Name', 'Age']])

    # Access rows using .iloc
    print(df.iloc[0])  # First row

    # Access rows using conditions
    print(df[df['Age'] > 25])

    # Filter rows where Age > 25
    filtered_df = df[df['Age'] > 25]
    print(filtered_df)

    # Add a new column
    df['Salary'] = [70000, 80000, 90000]
    print(df)

    # Modify an existing column
    df['Age'] = df['Age'] + 1
    print(df)