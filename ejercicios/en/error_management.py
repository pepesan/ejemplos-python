# -*- coding: utf-8 -*-

if __name__ == '__main__':

    while True:
        try:
            n = input("Please enter an integer: ")
            n = int(n)
            break
        except ValueError:
            print("Invalid integer! Please try again ...")
        finally:
            print("This will always execute")
    print("Great, you successfully entered an integer!")

    try:
        fh = open("testfile", "w")
        fh.write("This is my test file for exception handling!!")
    except IOError:
        print("Error: can't find file or read data")
    else:
        print("Content written to the file successfully")
        fh.close()

    try:
        x = float(input("Your number: "))
        print(x)
        inverse = 1.0 / x
    except ValueError:
        print("You should have entered either an int or a float")
    except ZeroDivisionError:
        print("Infinity")
    finally:
        print("An exception may or may not have occurred.")
