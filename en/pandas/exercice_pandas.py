if __name__ == '__main__':
    # -*- coding: utf-8 -*-

    # Importing pandas library
    import pandas as pd

    print(pd.__version__)

    # 1. Create a DataFrame
    data = {
        'Name': ['Alice', 'Bob', 'Charlie'],
        'Age': [25, 30, 35],
        'City': ['New York', 'Los Angeles', 'Chicago']
    }

    df = pd.DataFrame(data)
    print("Original DataFrame:")
    print(df)

    # 2. Filter rows where Age > 25
    filtered_df = df[df['Age'] > 25]
    print("\nFiltered DataFrame (Age > 25):")
    print(filtered_df)

    # 3. Add a Salary column and modify Age
    df['Salary'] = [70000, 80000, 90000]
    df['Age'] = df['Age'] + 1
    print("\nUpdated DataFrame:")
    print(df)

    # 4. Save the updated DataFrame to a CSV file
    output_file = 'output.csv'
    df.to_csv(output_file, index=False)
    print(f"\nData saved to {output_file}")

