if __name__ == '__main__':
    # Solution to Exercise 1: Reverse a String

    # Prompt the user to enter a string
    user_string = input("Enter a string: ")

    # Reverse the string using slicing
    reversed_string = user_string[::-1]

    # Display the reversed string
    print(f"Reversed string: {reversed_string}")

    # Solution to Exercise 2: Count Vowels in a String

    # Prompt the user to enter a string
    user_string = input("Enter a string: ")

    # Define a set of vowels
    vowels = {'a', 'e', 'i', 'o', 'u'}

    # Initialize a counter for vowels
    vowel_count = 0

    # Convert the string to lowercase to handle uppercase letters
    user_string = user_string.lower()

    # Iterate over each character in the string
    for char in user_string:
        # Check if the character is a vowel
        if char in vowels:
            vowel_count += 1

    # Display the number of vowels
    print(f"Number of vowels: {vowel_count}")

    # Solution to Exercise 3: Capitalize the First Letter of Each Word

    # Prompt the user to enter a sentence
    sentence = input("Enter a sentence: ")

    # Use the title() method to capitalize each word
    capitalized_sentence = sentence.title()

    # Display the capitalized sentence
    print(f"Capitalized sentence: {capitalized_sentence}")
