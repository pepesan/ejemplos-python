# -*- coding: utf-8 -*-

if __name__ == "__main__":
    # Escritura de un fichero
    cadena = "mi texto"
    fobj_out = open("ad_lesbiam.txt", "w")
    fobj_out.write(cadena)
    fobj_out.close()

    fobj_in = open("ad_lesbiam.txt")
    fobj_out = open("ad_lesbiam2.txt", "w")
    i = 1
    for line in fobj_in:
        print(line.rstrip())
        fobj_out.write(str(i) + ": " + line)
        i = i + 1
    fobj_in.close()
    fobj_out.close()


    #como listado
    lineas= open("ad_lesbiam.txt").readlines()
    print (lineas)
    #como string
    texto = open("ad_lesbiam.txt").read()
    print (texto[16:34])


    # lectura de fichero csv
    # Abrimos el archivo en modo lectura
    with open('archivo.csv', 'r') as archivo:
        # Leemos todas las líneas del archivo
        lineas = archivo.readlines()

    # Procesamos el contenido
    datos = []
    for linea in lineas:
        # Eliminamos saltos de línea y separamos los valores por comas
        fila = linea.strip().split(',')
        datos.append(fila)

    # Imprimimos el contenido
    for fila in datos:
        print(fila)



    # Example of writing and reading binary files without using functions

    # List of integers to store in the binary file
    numbers = [10, 20, 30, 40, 50]

    # ----- Writing to a binary file -----
    # Open the file in binary write mode ('wb')
    file_out = open("numbers.bin", "wb")

    # Write each integer to the file as binary data
    for number in numbers:
        # Convert the integer to bytes (4 bytes for each integer) and write to the file
        file_out.write(number.to_bytes(4, byteorder='little', signed=True))

    # Close the file after writing
    file_out.close()

    # ----- Reading from the binary file -----
    # Open the file in binary read mode ('rb')
    file_in = open("numbers.bin", "rb")

    # Initialize an empty list to store the integers read from the file
    read_numbers = []

    # Read the file 4 bytes at a time (size of an integer in bytes)
    while True:
        # Read 4 bytes from the file
        data = file_in.read(4)
        if not data:
            # Break the loop when there is no more data to read
            break
        # Convert the bytes back to an integer and append to the list
        number = int.from_bytes(data, byteorder='little', signed=True)
        read_numbers.append(number)

    # Close the file after reading
    file_in.close()

    # Print the list of integers read from the file
    print("List of integers read from the binary file:", read_numbers)