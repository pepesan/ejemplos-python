if __name__ == '__main__':
    # -*- coding: utf-8 -*-

    # Define lists
    list1 = ['physics', 'chemistry', 1997, 2000]
    list2 = [1, 2, 3, 4, 5, 5]
    list3 = ["a", "b", "c", "d"]

    # Access list elements
    print("list1[0]: ", list1[0])
    print("list2[1:5]: ", list2[1:5])

    # Get the length of the list
    print("len(list1)")
    print(len(list1))  # Output: 4

    # Update an element in a list
    newlist = ['physics', 'chemistry', 1997, 2000]
    print("Value available at index 2: ")
    print(newlist[2])
    newlist[2] = 2001
    print("New value available at index 2: ")
    print(newlist[2])

    # Append an element to the end of the list
    x = [1, 2, 3, 4]
    x.append(5)
    print(x)  # Output: [1, 2, 3, 4, 5]

    # Delete an element from a list
    list1 = ['physics', 'chemistry', 1997, 2000]
    print(list1)
    del (list1[2])
    print("After deleting value at index 2: ")
    print(list1)

    # Combine two lists
    print("Sum of two lists into one: ")
    list2 = list1 + x
    print(list2)

    # Append an element to a list
    print("Adding a value to a list, append")
    list3 = [1]
    list3.append(1)
    print(list3)

    # Work with lists of temperatures
    Celsius = [25.0, 36.5, 37.3, 37.8]
    UnFahrenheit = ((float(9) / 5) * Celsius[0] + 32)
    print("Celsius", Celsius)
    print("UnFahrenheit", UnFahrenheit)

    # Iterate through a list
    for element in Celsius:
        print(element)

    # Convert Celsius to Fahrenheit
    Fahrenheit = []
    for item in Celsius:
        i = ((float(9) / 5) * item + 32)
        Fahrenheit.append(i)
        print(i)
        print(Fahrenheit)

    print(Fahrenheit)

    # List comprehension for conversion
    Fahrenheit = [((float(9) / 5) * item + 32) for item in Celsius]
    print("List comprehension")
    print(Fahrenheit)

    # Generate Pythagorean triples
    milist = [(x, y, z)
              for x in range(1, 30)
              for y in range(x, 30)
              for z in range(y, 30)
              if x ** 2 + y ** 2 == z ** 2]
    print(milist)

    # Create ranges
    mi_listado = [x for x in range(1, 51)]
    print("range(1, 51)", mi_listado)
    mi_listado = [x for x in range(50)]
    print(range(50), mi_listado)
    print(len(mi_listado))

    # Iterate through a list
    listado = [1, 2, 3]
    for x in listado:
        print(x)

    # Check if an element is in a list
    if (3 in listado):
        print("It's alive!!!!!")

    # Multiply a list
    holas = ['Hi!'] * 4  # Output: ['Hi!', 'Hi!', 'Hi!', 'Hi!']
    print(4 * holas)

    # Work with nested lists
    print("Nested List")
    doubleList = []
    doubleList.append(2)
    print(doubleList)
    del doubleList[0]
    doubleList.append([1, 2])
    print(doubleList)
    doubleList.append([3, 4])
    print(doubleList)
    print("First row")
    print(doubleList[0])
    print("First element of the first row")
    print(doubleList[0][0])

    # Iterate through a nested list
    for row in doubleList:
        print(row)
        for column in row:
            print(column)

    doubleList.append(["Hello", "World"])
    print(doubleList)
    for fila in doubleList:
        print(fila)
        for columna in fila:
            print(columna)
