# importación declaraciones
from sqlalchemy.orm import declarative_base

# importar clase Base
Base = declarative_base()
# importaciones de operaciones y campos
from sqlalchemy import Column, Integer, String, select, update, Boolean


# declación de la clase/tabla User
class User(Base):
    __tablename__ = 'users'
    user_id = Column(Integer, primary_key=True)
    username = Column(String, nullable=False)
    password = Column(String, nullable=False)
    email = Column(String, nullable=False)
    name = Column(String)
    enctype = Column(String, nullable=False, default="sha1")
    active = Column(Boolean, nullable=False, default=False)

    def __repr__(self):
        return "<User(user_id='%s' name='%s', username='%s', password='******' email='%s', enctype='%s', active='%s')>" \
               % (self.user_id, self.name, self.username, self.email, self.enctype, self.active)


# importación de la conexión
from sqlalchemy import create_engine

# conexión a memoria sqlite
engine = create_engine('sqlite:///:memory:', echo=True)
# Volcado del esquema en la BBDD
Base.metadata.create_all(engine)

# Objeto entidad
ed_user = User(name='admin', username="admin", email="pepesan@gmail.com", password="admin", enctype="none", active=True)

# Manejo de sesiones
from sqlalchemy.orm import sessionmaker

Session = sessionmaker(bind=engine)
# objeto de sesión
session = Session()

# insertar usuario
session.add(ed_user)
session.flush()

# consulta con filtro
our_user = session.query(User).filter_by(name='admin').first()
print(our_user)

# Objeto entidad
ed_user = User(
    name='pepesan',
    username="pepesan",
    email="p@p.com",
    password="",
    enctype="None",
    active=False)
# insertar usuario
session.add(ed_user)
session.flush()
# consulta con filtro
our_user = session.query(User).filter_by(name='pepesan').first()
print(our_user)
# condiciones where
row = session.execute(
    select(User).where(User.username == "pepesan")
).first()
print(row)
#sentencia de update
session.execute(
    update(User).
        where(User.username == "pepesan").
        values(active=True)
)
# Comprobamos el cambio
row = session.execute(
    select(User).where(User.username == "pepesan")
).first()
print(row)
# borrado
pepesan = session.get(User, 2)
session.delete(pepesan)
session.flush()
row = session.execute(select(User)).all()
print(row)

# insert múltiple
session.add_all([
    User(username='wendy', email="", password="", enctype="None", active=True),
    User(username='mary', email="", password="", enctype="None", active=False),
    User(username='fred', email="", password="", enctype="None", active=True),
    User(username='juan', email="", password="", enctype="None", active=False),
    ])
row = session.execute(select(User)).all()
print(row)
# Comprobamos el cambio
row = session.execute(
    select(User).where(User.active == True)
).all()
print(row)
# ordenado de resultados con limit y offset
row = session.execute(
    select(User).order_by(User.username.desc()).limit(2).offset(0)
).all()
print(row)

# ordenado de resultados con limit y offset
row = session.execute(
    select(User).limit(2).offset(2)
).all()
print(row)
"""
# pendiente en sesión
print(session.new)
# flush de sesión
session.flush()
print(session.new)

# Consulta por ID
some_user = session.get(User, 4)
print(some_user)

# update

session.execute(
    update(User).
        where(User.name == "windy").
        values(fullname="Sandy Squirrel Extraordinaire")
)
session.flush()

# select con first
row = session.execute(select(User)).first()
print(row)
# selección de campos
row = session.execute(select(User.name, User.fullname)).first()
print(row)
# condiciones where
row = session.execute(
    select(User.name, User.fullname).where(User.name == "windy")
).all()
print(row)
# ordenado de resultados
row = session.execute(
    select(User).order_by(User.fullname.desc())
).all()
print(row)

# borrado
mary = session.get(User, 3)
session.delete(mary)
session.flush()
row = session.execute(select(User)).all()
print(row)

# limit y offset
row = session.execute(select(User).limit(2).offset(0)).all()
print(row)

# Agregando un usuario
mary = User(name='mary', fullname='Mary', nickname='mery')
session.add(mary)
# Haciendo rollback
session.rollback()
# Haciendo el commit
session.commit()
session.flush()
row = session.execute(select(User)).all()
print(row)

# Cerrado de sesión
session.close()
"""