if __name__ == '__main__':
    # functions.py

    # Function to subtract two numbers
    def subtract(x, y):
        """Returns the result of subtracting y from x."""
        return x - y


    # Call the function subtract and print the result
    result = subtract(5, 3)
    print("Result of subtracting 5 and 3:", result)


    # Function to divide two numbers
    def divide(x=1, y=1):
        """Returns the result of dividing x by y. Arguments are optional."""
        if y == 0:
            raise ValueError("Division by zero is not allowed.")
        return x / y


    # Call the function divide and print the results
    result = divide(5, 3)
    print("Result of dividing 5 by 3:", result)

    # Call divide with optional parameters
    print("Result with default parameters:", divide())
    print("Result with one parameter (5):", divide(5))
    print("Result with two parameters (5, 2):", divide(5, 2))


    # Function to count elements in a list
    def count(elements):
        """Prints the number of elements in a list."""
        if not isinstance(elements, list):
            raise TypeError("The argument must be a list.")
        print("Number of elements in the list:", len(elements))


    # Test the count function with a test list
    test_list = [1, 2, 3, 4, 5]
    count(test_list)
