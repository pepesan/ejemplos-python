from datetime import datetime

if __name__ == '__main__':
    # Ejemplo de cadena de texto
    fecha_str = "2024-11-05 14:30:00"

    # Convertir la cadena a un objeto datetime
    """
    %Y representa el año en cuatro dígitos.
    %m representa el mes en dos dígitos.
    %d representa el día en dos dígitos.
    %H representa la hora en formato de 24 horas.
    %M representa los minutos.
    %S representa los segundos.
    """
    fecha_obj = datetime.strptime(fecha_str, "%Y-%m-%d %H:%M:%S")

    print(fecha_obj)  # Salida: 2024-11-05 14:30:00

    # Ejemplo de objeto datetime
    fecha_obj = datetime(2024, 11, 5, 14, 30, 0)

    # Convertir el objeto datetime a una cadena
    fecha_str = fecha_obj.strftime("%d/%m/%Y %H:%M:%S")

    print(fecha_str)  # Salida: 05/11/2024 14:30:00

    # Fecha en formato inicial
    fecha_inicial = "05-11-2024 14:30:00"

    # Convertir a objeto datetime
    fecha_obj = datetime.strptime(fecha_inicial, "%d-%m-%Y %H:%M:%S")

    # Convertir a un nuevo formato de cadena
    fecha_nueva = fecha_obj.strftime("%Y/%m/%d %I:%M %p")

    print(fecha_nueva)  # Salida: 2024/11/05 02:30 PM

    from datetime import timedelta

    # Fecha actual
    fecha_actual = datetime.now()

    # Sumar 7 días
    fecha_futura = fecha_actual + timedelta(days=7)

    print("Fecha actual:", fecha_actual.strftime("%Y-%m-%d"))
    print("Fecha futura:", fecha_futura.strftime("%Y-%m-%d"))

    # Fecha en cadena
    fecha_inicial = "2024-11-05 14:30:00"

    # Convertir a objeto datetime
    fecha_obj = datetime.strptime(fecha_inicial, "%Y-%m-%d %H:%M:%S")

    # Restar 3 días
    fecha_modificada = fecha_obj - timedelta(days=3)

    # Convertir de nuevo a cadena en formato diferente
    fecha_final = fecha_modificada.strftime("%d-%m-%Y %I:%M %p")

    print("Fecha inicial:", fecha_inicial)
    print("Fecha modificada:", fecha_final)  # Salida: 02-11-2024 02:30 PM


