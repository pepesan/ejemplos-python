# pip install pymysql
import random
import string

import pymysql
from pymysql import Error


# Define la clase User asociada a la tabla
# Clases Entidad (ER)
class User:
    def __init__(self, user_id, email, password):
        self.user_id = user_id
        self.email = email
        self.password = password

    def __repr__(self):
        return f"User(id={self.user_id}, email='{self.email}', password='{self.password}')"



# Función para generar un email aleatorio
def generar_email_aleatorio():
    dominios = ["example.com", "test.com", "demo.com"]
    nombre = ''.join(random.choices(string.ascii_lowercase + string.digits, k=8))
    dominio = random.choice(dominios)
    return f"{nombre}@{dominio}"

if __name__ == '__main__':

    # Configuración de la conexión a la base de datos

    try:
        # Establecer la conexión
        connection = pymysql.connect(host='localhost',
                                     port=3306,
                                     user='root',
                                     password='root',
                                     database='test',
                                     cursorclass=pymysql.cursors.DictCursor)
        email_aleatorio = generar_email_aleatorio()
        with connection:
            print("conexión realizada")
            with connection.cursor() as cursor:
                print("cursor creado")
                # Create a new record
                sql = "INSERT INTO `users` (`email`, `password`) VALUES (%s, %s)"

                cursor.execute(sql, (email_aleatorio, 'very-secret'))

            # connection is not autocommit by default. So you must commit to save
            # your changes.
            connection.commit()

            with connection.cursor() as cursor:
                # Read a single record
                sql = "SELECT `id`,`email`, `password` FROM `users` WHERE `email`=%s"
                cursor.execute(sql, (email_aleatorio,))
                result = cursor.fetchone()
                print(result)

            users = []
            with connection.cursor() as cursor:
                # Define la consulta SQL
                sql = "SELECT id, email, password FROM users"
                cursor.execute(sql)

                # Recorre los resultados y crea objetos User
                for row in cursor.fetchall():
                    user_id, email, password = row
                    users.append(User(row[user_id], row[email], row[password]))
            print(users)


    except Error as err:
        print(f"Error: {err}")

    finally:
        # Cerrar el cursor y la conexión
        if cursor:
            cursor.close()
