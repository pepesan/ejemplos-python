if __name__ == '__main__':
    # -*- coding: utf-8 -*-

    # import the regular expressions module
    import re

    """Regular Expressions"""
    if re.search("cat", "A cat and a rat can't be friends."):
        print("Some kind of cat has been found :-)")
    else:
        print("No cat has been found :-(")

    if re.search("c[aeuio]t", "A cat and a rat can't be friends."):
        print("Some kind of c[vowel]t has been found :-)")
    else:
        print("No c[vowel]t has been found :-(")

    if re.search("[iI][pP][sS][uU][mM]", "Lorem IpSum  blah blah blah"):
        print("Found ipsum in uppercase or lowercase :-): ")
    else:
        print("Seems like it's not there :-(")

    # credit card comparison
    if re.search("(?:[0-9]{4}-){3}[0-9]{4}|[0-9]{16}", "5123-4567-8912-3456"):
        print("The credit card pattern matches")
    else:
        print("The credit card pattern does not match")

    # email comparison
    if re.search(r"[^@]+@[^@]+\.[^@]+", "p@p.com"):
        print("The email pattern matches")
    else:
        print("The email pattern does not match")
