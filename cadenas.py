if __name__ == '__main__':
    # -*- coding: utf-8 -*-

    # Ejemplos de cadenas en Python
    cadena = "Lorem Ipsum no cuetator cosas"
    cadena2 = " cx xc xc xc  otra cadena"

    # 1. Longitud de la cadena
    print("Longitud de la cadena:", len(cadena))

    # 2. Convertir a mayúsculas
    print("Cadena en mayúsculas:", cadena.upper())

    # 3. Convertir a minúsculas
    print("Cadena en minúsculas:", cadena.lower())

    # 4. Capitalizar (primera letra en mayúsculas)
    print("Cadena con capitalización:", cadena.capitalize())

    # 5. Título (primera letra de cada palabra en mayúsculas)
    print("Cadena en formato título:", cadena.title())

    # 6. Eliminar espacios en blanco al inicio y al final
    cadena_con_espacios = "    texto con espacios    "
    print("Cadena con espacios removidos:", cadena_con_espacios.strip())

    # 7. Concatenar dos cadenas
    cadena3 = cadena + cadena2
    print("Concatenación de cadenas:", cadena3)

    # 8. Encontrar una subcadena
    indice_subcadena = cadena3.find("otra")
    print("Índice de 'otra':", indice_subcadena)

    # 9. Reemplazar una subcadena por otra
    cadena_reemplazada = cadena3.replace("otra", "esta")
    print("Cadena cambiada:", cadena_reemplazada)

    # 10. Reemplazar una subcadena un número específico de veces
    cadena_reemplazada_parcial = cadena3.replace("xc", "22", 2)
    print("Cadena cambiada parcialmente:", cadena_reemplazada_parcial)

    # 11. Buscar el índice de una subcadena (lanza error si no se encuentra)
    try:
        indice = cadena3.index("xc")
        print("Índice de 'xc':", indice)
    except ValueError:
        print("Subcadena no encontrada")

    # 12. Contar el número de veces que aparece una subcadena
    conteo = cadena3.count("xc")
    print("Número de veces que aparece 'xc':", conteo)

    # 13. Verificar si una cadena empieza con otra cadena
    print("¿La cadena3 empieza con 'Lorem'?", cadena3.startswith("Lorem"))

    # 14. Verificar si una cadena termina con otra cadena
    print("¿La cadena3 termina con 'cadena'?", cadena3.endswith("cadena"))

    # 15. Convertir cadena en lista de palabras usando un delimitador (split)
    lista_palabras = cadena3.split()
    print("Lista de palabras:", lista_palabras)

    # 16. Unir una lista de palabras en una sola cadena con un delimitador (join)
    cadena_unida = "-".join(lista_palabras)
    print("Cadena unida con guiones:", cadena_unida)

    # 17. Comprobar si una cadena es numérica
    cadena_numerica = "12345"
    print("¿La cadena '12345' es numérica?", cadena_numerica.isdigit())

    # 18. Comprobar si una cadena es alfabética
    cadena_alfabetica = "abcdef"
    print("¿La cadena 'abcdef' es alfabética?", cadena_alfabetica.isalpha())

    # 19. Comprobar si una cadena es alfanumérica
    cadena_alfanumerica = "abc123"
    print("¿La cadena 'abc123' es alfanumérica?", cadena_alfanumerica.isalnum())

    # 20. Rellenar la cadena a la izquierda con ceros hasta una longitud
    cadena_ceros = cadena_numerica.zfill(10)
    print("Cadena rellenada con ceros:", cadena_ceros)

    # 21. Convertir a título y verificar si está en formato de título
    cadena_titulo = "hola mundo"
    cadena_titulo = cadena_titulo.title()
    print("Cadena en formato título:", cadena_titulo)
    print("¿Está en formato título?", cadena_titulo.istitle())

    # 22. Ejemplo de centrado, alineado a la izquierda y derecha
    print("Centrado:", cadena.center(40, "*"))
    print("Alineado a la izquierda:", cadena.ljust(40, "-"))
    print("Alineado a la derecha:", cadena.rjust(40, "-"))

    # 23. Obtener una subcadena usando slicing (corte)
    subcadena = cadena[0:5]
    print("Subcadena (primeros 5 caracteres):", subcadena)

    # 24. Invertir una cadena
    cadena_invertida = cadena[::-1]
    print("Cadena invertida:", cadena_invertida)

    # 25. Usar `format` para insertar valores en una cadena
    nombre = "Juan"
    edad = 25
    mensaje = "Mi nombre es {} y tengo {} años.".format(nombre, edad)
    print("Mensaje con formato:", mensaje)

    # 26. Usar f-strings para formatear la cadena (Python 3.6+)
    mensaje_fstring = f"Mi nombre es {nombre} y tengo {edad} años."
    print("Mensaje con f-strings:", mensaje_fstring)
