if __name__ == '__main__':
    # -*- coding: utf-8 -*-

    # Define an empty tuple
    tup1 = ()
    # Define a tuple with a single element
    tup1 = (50,)
    print(tup1)

    # Define tuples with multiple elements
    tup1 = ('physics', 'chemistry', 1997, 2000)
    tup2 = (1, 2, 3, 4, 5)
    tup3 = "a", "b", "c", "d"
    print("tup3")
    print(tup3)

    # Accessing tuple elements
    tup1 = ('physics', 'chemistry', 1997, 2000)
    tup2 = (1, 2, 3, 4, 5, 6, 7)
    print("tup1[0]")
    print(tup1[0])
    print("tup1[3]")
    print(tup1[3])

    # Access a slice of a tuple
    # Take only 4 elements, starting from the second position
    # From index 1 (inclusive) to 5 (exclusive)
    print(tup2[1:5])  # Output: (2, 3, 4, 5)

    # Tuples with mixed data types
    tup1 = (12, 34.56, True)
    tup2 = ('abc', 'xyz')

    # It is not possible to change a value within a tuple
    # using an assignment statement
    # Example: tup1[0] = 100  <- This is not allowed

    # Accessing and reassigning variables from tuple elements
    number = 2
    number = tup1[0]
    number = tup1[2]
    number = tup2[0]
    print(number)

    # Concatenate tuples
    tup1 = (12, 34.56, True)
    tup2 = ('abc', 'xyz')
    tup3 = tup1 + tup2
    print("tup3")
    print(tup3)  # Output: (12, 34.56, Tr
