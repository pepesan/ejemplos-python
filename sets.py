if __name__ == '__main__':
    my_set = {1, 2, 3, 4}
    # defining a set wit a list of elements
    my_set = set([1, 2, 3, 4,4])
    print(my_set)  # Output: {1, 2, 3, 4}
    my_set.add(5)  # Adding an element
    print(my_set)
    my_set.add(5)
    print(my_set)
    my_set.remove(3)  # Removing an element
    print(my_set)  # Output: {1, 2, 4, 5}

    set1 = {1, 2, 3}
    set2 = {3, 4, 5}
    print(set1 | set2)  # Union: {1, 2, 3, 4, 5}
    print(set1 & set2)  # Intersection: {3}
    print(set1 - set2)  # Difference: {1, 2}
